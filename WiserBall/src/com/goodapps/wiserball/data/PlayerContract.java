package com.goodapps.wiserball.data;

import android.provider.BaseColumns;

public class PlayerContract {
	public PlayerContract() {
	}

	public static abstract class PlayerEntry implements BaseColumns {
		public static final String TABLE_NAME = "player";
		public static final String COLUMN_PLAYER_NAME = "playername";
		public static final String COLUMN_TEAM_ID = "teamId";
		public static final String COLUMN_ROLE = "role";
		public static final String COLUMN_ORDER = "order_";
	}

	public static final String SQL_CREATE_ENTRIES = "CREATE TABLE " + PlayerEntry.TABLE_NAME + " ("
			+ PlayerEntry._ID + " INTEGER PRIMARY KEY, " + PlayerEntry.COLUMN_PLAYER_NAME
			+ " TEXT, " + PlayerEntry.COLUMN_ROLE + " INTEGER, " + PlayerEntry.COLUMN_ORDER
			+ " INTEGER, " + PlayerEntry.COLUMN_TEAM_ID + " INTEGER)";

	public static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS "
			+ PlayerEntry.TABLE_NAME;

	public long id;
	public long teamId;
	public String name;
	public int role;
	public int order;
}
