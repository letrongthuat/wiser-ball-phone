package com.goodapps.wiserball.game;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

import android.content.res.Resources;
import android.util.Log;

import com.goodapps.wiserball.R;
import com.goodapps.wiserball.SettingController;
import com.goodapps.wiserball.Utils;
import com.goodapps.wiserball.WBApplication;
import com.goodapps.wiserball.type.PlayerState;
import com.goodapps.wiserball.type.SetupState;
import com.goodapps.wiserball.type.TeamColor;
import com.goodapps.wiserball.type.TurnType;

public class WBMatch {
	public static final String TAG = "WBMatch";
	
	private List<WBTurn> _processedTurns;
	private List<WBTurn> _storedTurns;

	private int _numberOfPlayers;

	private boolean _isWhiteFirst;
	private boolean _isWhiteTurn;
	private WBTeam _teamRed;
	private WBTeam _teamWhite;
	private long _duration;
	private TeamColor _lastTeam;

	public WBMatch(int numberOfPlayers, boolean isWhiteFirst) {
		_processedTurns = new LinkedList<WBTurn>();
		_storedTurns = new LinkedList<WBTurn>();
		_numberOfPlayers = numberOfPlayers;

		_teamWhite = new WBTeam(TeamColor.White, numberOfPlayers);
		_teamRed = new WBTeam(TeamColor.White, numberOfPlayers);
		
		_lastTeam = TeamColor.Undefined;
		_teamWhite.setOpponent(_teamRed);
		_teamRed.setOpponent(_teamWhite);
		
		_isWhiteFirst = isWhiteFirst;
		_isWhiteTurn = isWhiteFirst;
		_duration = 0;
	}

	public WBTeam getRedTeam() {
		return _teamRed;
	}

	public WBTeam getWhiteTeam() {
		return _teamWhite;
	}

	public void addTurn(WBTurn turn) {
		if (_processedTurns.size() < _storedTurns.size()) {
			Collection<WBTurn> deleteTurn = _storedTurns.subList(_processedTurns.size(), _storedTurns.size());
			_storedTurns.removeAll(deleteTurn);
		}

		if (processTurn(turn))
			_storedTurns.add(turn);
	}

	private boolean processTurn(WBTurn turn) {
		if (turn == null || !WBTurn.isValidTurn(turn))
			return false;

		for (int i = 0; i < _numberOfPlayers; i++) {
			 _teamWhite.getPlayerAtNumber(i + 1).saveState();
			 _teamRed.getPlayerAtNumber(i + 1).saveState();
		}

		switch (turn.turnType) {
		case Setup:
			turn.starter.setSetupState(turn.setupState);
			break;
		case Hit:
			turn.starter.getTeam().setSkipTurn(0);
			processHitTurn(turn);
			break;
		case Indiv:
			turn.starter.getTeam().setSkipTurn(0);
			turn.starter.setIndivCount(turn.starter.getIndivCount() + 1);
		case Empty:
			break;
		case Miss:
			turn.starter.getTeam().setSkipTurn(0);
			break;
		case Out:
			turn.starter.getTeam().setSkipTurn(0);
			turn.starter.setPlayerState(PlayerState.Out);
			turn.starter.getTeam().removePlayer(turn.starter);
			break;
		case Skip: {
			WBTeam t = turn.starter.getTeam();
			t.setSkipTurn(t.getSkipTurn() + 1);

			if (t.getSkipTurn() > 3)
				t.setSkipTurnFault(t.getSkipTurn() - 3);

			break;
		}
		}

		_processedTurns.add(turn);

		if (_isWhiteTurn) {
			_lastTeam = TeamColor.White;
			_isWhiteTurn = !_isWhiteTurn;	
		} else {
			_lastTeam = TeamColor.Red;
			_isWhiteTurn = !_isWhiteTurn;
		}

		return true;
	}

	private void processHitTurn(WBTurn turn) {
		List<WBPlayer> hitTeammates = new LinkedList<WBPlayer>();
		List<WBPlayer> hitOpponents = new LinkedList<WBPlayer>();

		for (int i = 0; i < turn.hitChain.size(); i++) {
			WBPlayer player = (WBPlayer) turn.hitChain.get(i);

			// if the player hit his team mates then hit an opponent
			if (hitTeammates.size() > 0 && player.getTeam() != turn.starter.getTeam())
				break;

			if (player.getTeam() == turn.starter.getTeam()) {
				if (hitOpponents.size() == 0)
					hitTeammates.add(player);
				// else -> nothing
				// if the player has hit the opponent, team hit is nothing
			} else {
				if (hitTeammates.size() == 0)
					hitOpponents.add(player);
				else
					break;
			}
		}

		// if hit the teammates
		if (hitTeammates.size() > 0) {
			turn.starter.getTeam().addHitReceivedFromTeam(turn.starter, hitTeammates);
		} else if (hitOpponents.size() > 0) {
			
			if (turn.starter.getTeam().getSkipTurnFault() > 0)
			{
				int skipTurnFault = turn.starter.getTeam().getSkipTurnFault();
				int invalidCount = Math.min(skipTurnFault, hitOpponents.size());
				for (int i = 0; i < invalidCount; i++)
					hitOpponents.remove(0);
				turn.starter.getTeam().setSkipTurnFault(skipTurnFault - invalidCount);
			}
			int startIndex = 1;
			if (turn.starter.getHitOpponents().size() > 0)
			{
				List<Integer> opponentIndexes = turn.starter.getHitOpponentIndexes();
				startIndex = opponentIndexes.get(opponentIndexes.size() - 1) + 1;
			}
			for (int i = 0; i < hitOpponents.size(); i++) {
				WBPlayer opponent = hitOpponents.get(i);
				turn.starter.getHitOpponents().add(opponent);
				turn.starter.getHitOpponentIndexes().add(Integer.valueOf(startIndex + i));
			}

			turn.starter.getTeam().addHitSendingToOpponents(turn.starter, hitOpponents);
			turn.starter.getTeam().getOpponent()
					.addHitReceivedFromOpponent(turn.starter, hitOpponents);
		}
	}

	public int getCurrentTurnIndex() {
		return _processedTurns.size();
	}

	public void setCurrentTurnIndex(int index) {
		_processedTurns.clear();
		_isWhiteTurn = _isWhiteFirst;
		_teamRed.resetTeam();
		_teamWhite.resetTeam();

		for (int i = 0; i < index; i++)
			processTurn(_storedTurns.get(i));
	}

	public int getTurnCount() {
		return _storedTurns.size();
	}

	public void backTurn() {
		if (canBack()) {
			int turnCount = _processedTurns.size() - 1;
			_processedTurns.clear();

			_isWhiteTurn = _isWhiteFirst;
			_teamRed.resetTeam();
			_teamWhite.resetTeam();

			for (int i = 0; i < turnCount; i++)
				processTurn(_storedTurns.get(i));
		}
	}

	public void nextTurn() {
		if (canNext())
			processTurn(_storedTurns.get(_processedTurns.size()));
	}

	public boolean canBack() {
		return getCurrentTurnIndex() > 0;
	}

	public boolean canNext() {
		return getCurrentTurnIndex() < _storedTurns.size();
	}

	public boolean isWhiteTurn() {
		return _isWhiteTurn;
	}

	public String getDescriptionOfTurn(WBTurn turn) {
		StringBuilder toString = new StringBuilder();
	    
	    if (turn == null)
	    	return toString.toString();
	    
	    if (turn.starter != null) {
	        toString.append(getTextOfPlayer(turn.starter));
	        toString.append(" --> ");
	    }
	    
	    Resources defaultRes = WBApplication.getSharedApplication().getResources();
	    int language = new SettingController(WBApplication.getSharedApplication()).getLanguage();
	    Resources res = Utils.getResourcesForLocale(defaultRes, Utils.getLocale(language));
	    
	    switch (turn.turnType)
	    {
	        case Empty:
	        	toString.append(res.getString(R.string.label_change_turn));
	            break;
	        case Hit:
	        	if (turn.isHitToTeammates())
	        		toString.append(res.getString(R.string.label_mishit));
	        	else
	        		toString.append(res.getString(R.string.label_hit));
	            break;
	        case Indiv:
	            toString.append(res.getString(R.string.label_indiv_turn));
	            break;
	        case Miss:
	            toString.append(res.getString(R.string.label_miss));
	            break;
	        case Skip:
	            toString.append(res.getString(R.string.label_skip_turn));
	            break;
	        case Setup:
	            if (turn.setupState == SetupState.Defence)
	                toString.append(res.getString(R.string.label_defence));
	            else if (turn.setupState == SetupState.Attack)
	                toString.append(res.getString(R.string.label_attack));
	        default:
	            break;
	    }
	    
	    toString.append(" ");
	    if (turn.turnType == TurnType.Hit)
	    {
	        toString.append(this.getTextOfPlayer(turn.hitChain.get(0)));
	        for (int i = 1; i < turn.hitChain.size(); i++)
	        {
	            toString.append(", ");
	            toString.append(this.getDisplayTextOfPlayer(turn.hitChain.get(i)));      
	        }
	    }

	    return toString.toString();
	}

	public String getTextOfPlayer(WBPlayer player) 
	{
		if (player.getTeam() == _teamRed)
			return String.format("R%d", player.getPlayerNumber());
		else
			return String.format("W%d", player.getPlayerNumber());
	}

	public String getDisplayTextOfPlayer(WBPlayer player)
	{
		Resources defaultRes = WBApplication.getSharedApplication().getResources();
	    int language = new SettingController(WBApplication.getSharedApplication()).getLanguage();
	    Resources res = Utils.getResourcesForLocale(defaultRes, Utils.getLocale(language));
	    
	    if (player.getTeam().getTeamColor() == TeamColor.Red)
	    	return String.format("%s%d", res.getString(R.string.label_red), player.getPlayerNumber());
	    else 
	    	return String.format("%s%d", res.getString(R.string.label_white), player.getPlayerNumber());
	}
	
	public String getBallRescuedText(WBTeam team) {
		StringBuilder text = new StringBuilder();
	    for (int i = 0; i < _numberOfPlayers; i++)
	    {
	        WBPlayer player = team.getPlayerAtNumber(i + 1);
	        String stateChanges = player.getStateChangedText();
	        
	        if (stateChanges.length() > 0)
	        {
	        	text.append(getTextOfPlayer(player));
	        	text.append(": ");
	            text.append(stateChanges);
	            text.append(", ");
	        }
	    }
	    
	    return text.toString();
	}
	
	public WBPlayer getPlayerFromText(String text) {
		String normalized = text.trim();

		int index = normalized.charAt(1) - 48;
		WBPlayer player = null;

		switch (normalized.charAt(0)) {
		case 'W':
			player = _teamWhite.getPlayerAtNumber(index);
			break;

		case 'R':
			player = _teamRed.getPlayerAtNumber(index);
			break;
		}

		return player;
	}

	public WBTurn getLastTurn() {
		if (_processedTurns.size() > 0)
			return _processedTurns.get(_processedTurns.size() - 1);
		return null;
	}

	public TeamColor getWinnerTeam() {
		boolean checkRed = false;
		boolean checkWhite = false;
		for (int i = 1; i <= _teamRed.getNumberOfPlayers(); i++) {
			if (_teamRed.getPlayerAtNumber(i).getPlayerState() == PlayerState.Green) {
				checkRed = true;
				break;
			}
		}

		for (int i = 1; i <= _teamRed.getNumberOfPlayers(); i++) {
			if (_teamWhite.getPlayerAtNumber(i).getPlayerState() == PlayerState.Green) {
				checkWhite = true;
				break;
			}
		}

		if ((checkWhite && checkRed) || (!checkRed && !checkWhite)) {
			return TeamColor.Undefined;
		}

		if (checkWhite)
			return TeamColor.White;

		if (checkRed)
			return TeamColor.Red;

		return TeamColor.Undefined;
	}

	public int getNumberOfPlayers() {
		return _numberOfPlayers;
	}

	public String getCurrentDescription() {
		return "";
	}

	public long getDuration() {
		return _duration;
	}

	public boolean isSetupComplete() {
		for (int i = 1; i <= _numberOfPlayers; i++) {
			if (_teamRed.getPlayerAtNumber(i).getSetupState() == SetupState.Undefined)
				return false;
			if (_teamWhite.getPlayerAtNumber(i).getSetupState() == SetupState.Undefined) 
				return false;
		}
		
		return true;
	}
	
	public String getTextFromTurn(WBTurn turn) {
		if (turn == null)
	        return "";
	    
	    StringBuilder toString = new StringBuilder();
	    switch (turn.turnType)
	    {
	        case Empty:
	            toString.append("C");
	            break;
	        case Hit:
	        	toString.append("H");
	            break;
	        case Indiv:
	        	toString.append("I");
	            break;
	        case Miss:
	        	toString.append("M");
	            break;
	        case Skip:
	        	toString.append("S");
	            break;
	        case Setup:
	        	toString.append("E");
	        default:
	            break;
	    }

	    if (turn.starter != null)
	    	toString.append(" " + getTextOfPlayer(turn.starter));

	    if (turn.turnType == TurnType.Setup)
	    {
	        if (turn.setupState == SetupState.Defence)
	            toString.append(" D");
	        else
	        	toString.append(" A");
	    }
	    else
	        for (int i = 0; i < turn.hitChain.size(); i++)
	        {
	            WBPlayer player = turn.hitChain.get(i);
	            toString.append(" " + getTextOfPlayer(player));
	        }

	    return toString.toString();
	}

	
	public WBTurn getTurnFromText(String text) {
		String[] components = text.split(" ");

		TurnType turnType = TurnType.Empty;
	    
	    switch (components[0].charAt(0))
	    {
	        case 'C':
	            turnType = TurnType.Empty;
	            break;
	        case 'H':
	            turnType = TurnType.Hit;
	            break;
	        case 'I':
	            turnType = TurnType.Indiv;
	            break;
	        case 'M':
	            turnType = TurnType.Miss;
	            break;
	        case 'S':
	            turnType = TurnType.Skip;
	            break;
	        case 'E':
	            turnType = TurnType.Setup;
	            break;
	    };
	    
	    WBPlayer starter = getPlayerFromText(components[1]);
	    
	    WBTurn turn = new WBTurn(starter, turnType);
	    
	    if (turnType == TurnType.Setup)
	    {
	        if (components[2].charAt(0) == 'D')
	            turn.setupState = SetupState.Defence;
	        else
	            turn.setupState = SetupState.Attack;
	    }
	    else
	        for (int i = 2; i < components.length; i++)
	        {
	            WBPlayer player = getPlayerFromText(components[i]);
	            turn.hitChain.add(player);
	        }
	    
	    return turn;
	}
	
	public boolean isWhiteFirst() {
		return _isWhiteFirst;
	}
	
	public void saveToFile(String path) throws IOException {
		StringBuilder content = new StringBuilder();
		content.append("number_of_players: " + _numberOfPlayers + "\n");
		content.append("number_of_turns: " + getTurnCount() + "\n");
		content.append("team_first: " + (_isWhiteFirst ? 1 : 0) + "\n");
	    content.append("red_team_name: " + ((_teamRed.getName() == null || _teamRed.getName().trim() == "") ? "RED" : _teamRed.getName()) + "\n");
	    content.append("white_team_name: " + ((_teamWhite.getName() == null || _teamWhite.getName().trim() == "") ? "WHITE" : _teamWhite.getName()) + "\n");
	    
	    content.append("red_team_fault: " + _teamRed.getTeamFault() + "\n");
	    content.append("white_team_fault: " + _teamWhite.getTeamFault() + "\n");
	    
	    for (int i = 0; i < _numberOfPlayers; i++) {
	    	String name = _teamRed.getPlayerAtNumber(i + 1).getName();
	        content.append("red_player_name: " + ((name == null || name.trim() == "") ? (i + 1) : name) + "\n");
	    }
	    
	    for (int i = 0; i < _numberOfPlayers; i++) {
	    	String name = _teamWhite.getPlayerAtNumber(i + 1).getName();
	        content.append("white_player_name: " + ((name == null || name.trim() == "") ? (i + 1) : name) + "\n");
	    }
	    
	    for (int i = 0; i < getTurnCount(); i++)
	        content.append("turn: " + getTextFromTurn(_storedTurns.get(i)) + "\n");
	
		File file = new File(path);
		
		if (file.createNewFile())
		{
			file.delete();
			file.createNewFile();
		}
		
		FileWriter fw = new FileWriter(file);
		fw.append(content.toString());
		fw.close();
	}
	
	public static WBMatch openFromFile(String fileName) throws IOException {
	    
		File file = new File(fileName);
		
		Scanner scanner = new Scanner(file);
		scanner.useDelimiter(System.getProperty("line.separator"));
		
		scanner.skip("number_of_players: ");
		int numberOfPlayers = scanner.nextInt();
		Log.i(TAG, "Number Of Players: " + numberOfPlayers);
		scanner.skip("\n");
		
		scanner.skip("number_of_turns: ");
		int turnCount = scanner.nextInt();
		Log.i(TAG, "Number Of Turns: " + turnCount);
		scanner.skip("\n");
	    
		scanner.skip("team_first: ");
	    boolean isWhiteFirst = scanner.nextInt() == 1;
	    Log.i(TAG, "Is White First: " + isWhiteFirst);
	    scanner.skip("\n");
	    
	    WBMatch match = new WBMatch(numberOfPlayers, isWhiteFirst);

	    scanner.skip("red_team_name: ");
	    String redTeamName = scanner.next();
	    Log.i(TAG, "Red Team Name: " + redTeamName);
	    match.getRedTeam().setName(redTeamName);
	    scanner.skip("\n");
	    
	    scanner.skip("white_team_name: ");
	    String whiteTeamName = scanner.next();
	    match.getWhiteTeam().setName(whiteTeamName);
	    Log.i(TAG, "White Team Name: " + whiteTeamName);
	    scanner.skip("\n");
 
	    scanner.skip("red_team_fault: ");
	    int redTeamFault = scanner.nextInt();
	    match.getRedTeam().setTeamFault(redTeamFault);
	    Log.i(TAG, "Red Team Fault: " + redTeamFault);
	    scanner.skip("\n");
	    
	    scanner.skip("white_team_fault: ");
	    int whiteTeamFault = scanner.nextInt();
	    match.getWhiteTeam().setTeamFault(whiteTeamFault);
	    Log.i(TAG, "White Team Fault: " + whiteTeamFault);
	    scanner.skip("\n");
	    
	    for (int i = 0; i < numberOfPlayers; i++)
	    {
	    	scanner.skip("red_player_name: ");
	    	String playerName = scanner.next();
	    	match.getRedTeam().getPlayerAtNumber(i+ 1).setName(playerName);
	    	scanner.skip("\n");
	        Log.i(TAG, "Red Player " + (i + 1) + ": " + playerName);
	    }
	    
	    for (int i = 0; i < numberOfPlayers; i++)
	    {
	    	scanner.skip("white_player_name: ");
	    	String playerName = scanner.next();
	    	match.getWhiteTeam().getPlayerAtNumber(i+ 1).setName(playerName);
	    	scanner.skip("\n");
	        Log.i(TAG, "White Player " + (i + 1) + ": " + playerName);
	    }
	    

	    
	    Pattern pattern = Pattern.compile(".*");
	    for (int i = 0; i < turnCount; i++)
	    {
	    	scanner.skip("turn: ");
	    	String turnText = scanner.findInLine(pattern);
	        Log.i(TAG, "Turn " + (i + 1) + ": " + turnText);
	        WBTurn turn = match.getTurnFromText(turnText);
	        match.addTurn(turn);
	    }
	    
	    scanner.close();
	    return match;
	}
	
	public int getTotalTurnsOfPlayer(int number, WBTeam team) {
	    int total = getHitTurnsOfPlayer(number, team);
	    total += getMissTurnsOfPlayer(number, team);
	    total += getSkipTurnsOfPlayer(number, team);
	    total += getFaultTurnsOfPlayer(number, team);
	    
	    return total;
	}

	public int getHitTurnsOfPlayer(int number, WBTeam team) {
	    int count = 0;
	    
	    WBPlayer player = team.getPlayerAtNumber(number);
	    for (int i = 0; i < _storedTurns.size(); i++) {
	        WBTurn turn = _storedTurns.get(i);
	        if (player == turn.starter && turn.turnType == TurnType.Hit)
	        	for (int j = 0; j < turn.hitChain.size(); j++)
	        	{
	        		WBPlayer hitted = turn.hitChain.get(j);
	        		if (hitted.getTeam() != team)
	        			count++;
	        		else
	        			break;
	        	}
	    }
	    
	    return count;
	}

	public int getMissTurnsOfPlayer(int number, WBTeam team) {
	    int count = 0;
	    
	    WBPlayer player = team.getPlayerAtNumber(number);
	    for (int i = 0; i < _storedTurns.size(); i++) {
	        WBTurn turn = _storedTurns.get(i);
	        if (player == turn.starter && turn.turnType == TurnType.Miss)
	            count++;
	    }
	    
	    return count;
	}

	public int getSkipTurnsOfPlayer(int number, WBTeam team)
	{
	    int count = 0;
	    
	    WBPlayer player = team.getPlayerAtNumber(number);
	    for (int i = 0; i < _storedTurns.size(); i++) {
	        WBTurn turn = _storedTurns.get(i);
	        if (player == turn.starter && turn.turnType == TurnType.Skip)
	            count++;
	    }
	    
	    return count;
	}

	public int getFaultTurnsOfPlayer(int number, WBTeam team) {
	    int count = 0;
	    
	    WBPlayer player = team.getPlayerAtNumber(number);
	    for (int i = 0; i < _storedTurns.size(); i++) {
	        WBTurn turn = _storedTurns.get(i);
	        if (player == turn.starter && turn.turnType == TurnType.Indiv)
	            count++;
	    }
	    
	    return count;
	}
	
	public TeamColor getLastTeamColor() {
		return _lastTeam;
	}
	
	public int getTurnCountOfTeam(WBTeam team) {
		int count = 0;
		
		for (int i = 2 * team.getNumberOfPlayers(); i < _storedTurns.size(); i++) {
			WBTurn turn = _storedTurns.get(i);
			if (turn.starter.getTeam() == team) 
				count++;
		}
		
		return count;
	}
}
