package com.goodapps.wiserball.game;

import java.util.LinkedList;
import java.util.List;

import com.goodapps.wiserball.type.PlayerRole;
import com.goodapps.wiserball.type.PlayerState;
import com.goodapps.wiserball.type.SetupState;
import com.goodapps.wiserball.type.TeamColor;

public class WBTeam {
	private int _skipTurn;
	private int _skipTurnFault;
	private WBPlayer _players[];
	private int _numberOfPlayers;
	private TeamColor _teamColor;
	private WBTeam _opponent;
	private String _name;
	private int _teamFault;
	
	private List<List<WBPlayer>> _receivedHitFromTeamMatesSequence;
	private List<Integer> _receivedHitFromTeamMatesIndexes;
	
	private List<WBPlayer> _receivedHitFromOpponentsSequence;
	private int _accumulatedFreeOpponentHits;
	
	public WBTeam(TeamColor teamColor, int number) {
		_numberOfPlayers = number;
		_teamColor = teamColor;
		
		_receivedHitFromTeamMatesSequence = new LinkedList<List<WBPlayer>>();
		_receivedHitFromTeamMatesIndexes = new LinkedList<Integer>();
		
		_receivedHitFromOpponentsSequence = new LinkedList<WBPlayer>();
		_accumulatedFreeOpponentHits = 0;
		_teamFault = 0;
		_players = new WBPlayer[number];
		
		for (int i = 0; i < _numberOfPlayers; i++) {
			_players[i] = new WBPlayer(this);
			_players[i].setPlayerNumber(i + 1);
			_players[i].setPlayerState(PlayerState.Green);
			_players[i].setSetupState(SetupState.Undefined);
		}
	}
	
	public String getName() {
		return _name;
	}
	
	public void setName(String name) {
		_name = name;
	}
	
	public void resetTeam() {
		_receivedHitFromTeamMatesSequence.clear();
		_receivedHitFromTeamMatesIndexes.clear();
		
	    _receivedHitFromOpponentsSequence.clear();
	    _skipTurn = 0;
	    _skipTurnFault = 0;
	    _accumulatedFreeOpponentHits = 0;
	    _teamFault = 0;
	    for (int i = 0; i < _numberOfPlayers; i++)
	        _players[i].resetState();
	}

	public WBTeam getOpponent() {
		return _opponent;
	}
	
	public void rescueHitReceivedFromOpponent(WBPlayer opponent) {
		if (opponent.getHitOpponents().size() > 0) {
			WBPlayer rescuedPlayer = opponent.getHitOpponents().get(0);

			int index = _receivedHitFromOpponentsSequence.indexOf(rescuedPlayer);
	        	        
	        if (opponent.getPlayerNumber() != rescuedPlayer.getPlayerNumberOfFirstHitReceivedFromOpponent()) {
	        	index = _receivedHitFromOpponentsSequence.lastIndexOf(rescuedPlayer);
	        }
	        
	        _receivedHitFromOpponentsSequence.remove(index);

	        rescuedPlayer.removeHitReceivedFromOpponent(opponent);
	    }
	}
	
	public int rescueHitReceivedFromOutOpponentWithMaximum(int maximum) {
	    int rescueCount = 0;
	    int i = 0;
	    while (i < _receivedHitFromOpponentsSequence.size() && rescueCount < maximum) {
	        WBPlayer wasHitPlayer = _receivedHitFromOpponentsSequence.get(i);
	        
	        int opponentNumber = 0;
	        int index = 0;
	        
	        int findIndex = _receivedHitFromOpponentsSequence.indexOf(wasHitPlayer);
	        
	        if (findIndex != -1 && findIndex < i) {
	        	opponentNumber = wasHitPlayer.getPlayerNumberOfFirstHitReceivedFromOpponent();
	        	index = 0;
	        } else {
	        	opponentNumber = wasHitPlayer.getPlayerNumberOfSecondHitReceivedFromOpponent();
	        	index = 1;
	        }

	        if (opponentNumber > 0 && _opponent.getPlayerAtNumber(opponentNumber).getPlayerState() == PlayerState.Out) {
	        	wasHitPlayer.removeHitReceivedFromOutOpponentAtIndex(index);
	        	_receivedHitFromOpponentsSequence.remove(i);
	        } else {
	        	i++;
	        }
	    }
	        
	    return  rescueCount;
	}
	    
	public void addHitReceivedFromOpponent(WBPlayer opponent, List<WBPlayer> players) {
	    for (int i = 0; i < players.size(); i++) {
	    	WBPlayer player = players.get(i);
	    	player.addHitReceivedFromOpponent(opponent);
	    	_receivedHitFromOpponentsSequence.add(player);
	    	if (player.getPlayerState() == PlayerState.Out)
	    		removePlayer(player);
	    }
	}
	
	public void addHitReceivedFromTeam(WBPlayer player, List<WBPlayer> players) {
		if (players.size() == 0) 
			return;
		
		//remove starter
		player.setPlayerState(PlayerState.Out);
		removePlayer(player);
		
		for (int i = 0; i < players.size(); i++) {
			WBPlayer p = players.get(i);
			if (p.getPlayerState() == PlayerState.Red) {
				removePlayer(p);
				p.setPlayerState(PlayerState.Out);
				players.remove(p);
			} else {
				p.addHitReceivedFromTeamMates(players);
			}
		}
		
		if (players.size() > 0) 
		{
			_receivedHitFromTeamMatesSequence.add(players);
			
			int index = 1;
	        if (_receivedHitFromTeamMatesIndexes.size() > 0)
	        	index = _receivedHitFromTeamMatesIndexes.get(_receivedHitFromTeamMatesIndexes.size() - 1) + 1;

	        _receivedHitFromTeamMatesIndexes.add(index);
		}
	}
	
	public boolean rescueTeamHitReceived() {
	    if (_receivedHitFromTeamMatesSequence.size() == 0)
	        return false;
	    
	    List<WBPlayer> teamHit = _receivedHitFromTeamMatesSequence.get(0);
	    while (_accumulatedFreeOpponentHits > 0 && teamHit.size() <= _accumulatedFreeOpponentHits)
	    {
	        for (int i = 0; i < teamHit.size(); i++) {
	            WBPlayer player = teamHit.get(i);
	            if (player.getPlayerState() != PlayerState.Out)
	            	player.removeHitReceivedFromTeamMates();
	        }

	        _receivedHitFromTeamMatesSequence.remove(0);
	        _receivedHitFromTeamMatesIndexes.remove(0);
	        _accumulatedFreeOpponentHits -= teamHit.size();
	        
	        if (_receivedHitFromTeamMatesSequence.size() > 0)
	        	teamHit = _receivedHitFromTeamMatesSequence.get(0);
	    }
	    
	    if (_receivedHitFromTeamMatesSequence.size() == 0)
	        _accumulatedFreeOpponentHits = 0;
	    
	    return true;
	}
	
	public int  getIndexOfTeamHitReceived(List<WBPlayer> teamHit) {
		int hitIndex = 0;
		for (int i = 0; i < _receivedHitFromTeamMatesSequence.size(); i++) {
			if (_receivedHitFromTeamMatesSequence.get(i) == teamHit)
				hitIndex = i;
		}
//		int hitIndex = _receivedHitFromTeamMatesSequence.indexOf(teamHit);
	    return _receivedHitFromTeamMatesIndexes.get(hitIndex);
	}
	
	public String getStringOfHitReceivedFromTeam(List<WBPlayer> teamHit)
	{
	    String result = "";
	    
	    int index = getIndexOfTeamHitReceived(teamHit);
	    int discount = 0;
	    if (index == 1) {
	    	discount = _accumulatedFreeOpponentHits;
	    }
	    
	    for (int i = 0; i < teamHit.size() - discount; i++)
	    	result += String.format("%c", index + 0x40);

	    return result;
	}
	
	public void removePlayer(WBPlayer player) {
		
	    if (player.getRole() == PlayerRole.Captain) {
	    	boolean hasAlter = false;
	        
	        for (int i = 0; i < _numberOfPlayers; i++) {
	            if (_players[i].getPlayerState() != PlayerState.Out) {
	                _players[i].setRole(PlayerRole.Captain);
	                hasAlter = true;
	                break;
	            }
	        }
	        
	        if (hasAlter)
	            player.setRole(PlayerRole.Normal);
	    }
	    
	    for (int i = 0; i < _receivedHitFromTeamMatesSequence.size(); i++) {
	        List<WBPlayer> teamHit = _receivedHitFromTeamMatesSequence.get(i);
	        teamHit.remove(player);
	        if (teamHit.size() == 0)
	            _receivedHitFromTeamMatesSequence.remove(teamHit);
	    }
	    
	    _receivedHitFromOpponentsSequence.remove(player);
	    
	    for (int i = 0; i < _opponent.getNumberOfPlayers(); i++) {
	    	WBPlayer opponent = _opponent.getPlayerAtNumber(i + 1);
	    	opponent.getHitOpponents().remove(player);
	    }
	}
	
	public void addHitSendingToOpponents(WBPlayer player, List<WBPlayer> opponents) {
	    //team rescue && out opponent hit rescue
	    int freeOpponentHits = 0;
	    
	    //rescue hit of opponent
	    for (int i = 0; i < opponents.size(); i++) {
	        WBPlayer opponent = opponents.get(i);
	        if (opponent.getHitOpponents().size() != 0)
	        	rescueHitReceivedFromOpponent(opponent);
	        else
	            freeOpponentHits++;
	    }
	    
	    int numberOfRescuePlayers = rescueHitReceivedFromOutOpponentWithMaximum(freeOpponentHits);
	    
	    if (_receivedHitFromTeamMatesSequence.size() > 0) {
	        _accumulatedFreeOpponentHits += freeOpponentHits - numberOfRescuePlayers;
	    
	        rescueTeamHitReceived();
	    }
	}
	
	public int getNumberOfActacks() {
		int count = 0;
		for (int i = 0;  i < _numberOfPlayers; i++)
			if (_players[i].getSetupState() == SetupState.Attack) 
				count++;
		return count;
	}
	
	public int getNumberOfDefences() {
		int count = 0;
		for (int i = 0; i < _numberOfPlayers; i++) 
			if (_players[i].getSetupState() == SetupState.Defence)
				count++;
		return count;
	}
	
	public void setSkipTurn(int skipTurn) {
		_skipTurn = skipTurn;
	}
	
	public int getSkipTurn() {
		return _skipTurn;
	}
	
	public WBPlayer getPlayerAtNumber(int number) {
		return _players[number - 1];
	}
	
	public int getNumberOfPlayers() {
		return _numberOfPlayers;
	}
	
	public TeamColor getTeamColor() {
		return _teamColor;
	}
	
	public int getSkipTurnFault() {
		return _skipTurnFault;
	}
	
	public void setSkipTurnFault(int skipTurnFault) {
		_skipTurnFault = skipTurnFault;
	}
	
	public void setOpponent(WBTeam team) {
		_opponent = team;
	}
	
	public void setTeamFault(int teamFault) {
		_teamFault = teamFault;
	}
	
	public int getTeamFault() {
		return _teamFault;
	}
	
}
