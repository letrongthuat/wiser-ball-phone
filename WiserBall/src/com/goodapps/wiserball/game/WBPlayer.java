package com.goodapps.wiserball.game;

import java.util.LinkedList;
import java.util.List;

import com.goodapps.wiserball.type.PlayerRole;
import com.goodapps.wiserball.type.PlayerState;
import com.goodapps.wiserball.type.SetupState;

public class WBPlayer {
	private WBTeam _team;
	private List<List<WBPlayer>> _receivedHitFromTeamMates;
	private List<WBPlayer> _receivedHitFromOpponents;
	private List<WBPlayer> _hitOpponents;
	private List<Integer> _hitIndexes;

	private PlayerState _playerState;
	private SetupState _setupState;
	private PlayerState _oldState;
	private int _playerNumber;
	private int _indivCount;
	private String _name;
	private PlayerRole _role;

	public WBPlayer(WBTeam team) {
		_team = team;
		_receivedHitFromTeamMates = new LinkedList<List<WBPlayer>>();
		_receivedHitFromOpponents = new LinkedList<WBPlayer>();
		_hitOpponents = new LinkedList<WBPlayer>();
		_hitIndexes = new LinkedList<Integer>();
		
		_playerState = PlayerState.Green;
		_oldState = _playerState;
		_setupState = SetupState.Undefined;
		_indivCount = 0;
	}

	public void resetState() {
		_receivedHitFromTeamMates.clear();
		_receivedHitFromOpponents.clear();
		
		_hitOpponents.clear();
		_hitIndexes.clear();
		
		_playerState = PlayerState.Green;
		_oldState = _playerState;
		_setupState = SetupState.Undefined;
		_indivCount = 0;
	}

	public void setName(String name) {
		_name = name;
	}
	
	public String getName() {
		return _name;
	}
	
	public void addHitReceivedFromTeamMates(List<WBPlayer> teamHit) {
		if (_playerState == PlayerState.Out)
			return;
		
		setPlayerState(PlayerState.fromInt(getPlayerState().toInt() + 1));
		_receivedHitFromTeamMates.add(teamHit);
	}

	public void removeHitReceivedFromTeamMates() {
		if (_playerState == PlayerState.Out || _playerState == PlayerState.Green)
			return;
		
		if (_receivedHitFromTeamMates.size() > 0) {
			setPlayerState(PlayerState.fromInt(getPlayerState().toInt() - 1));
			_receivedHitFromTeamMates.remove(0);
		}
	}

	public int getFirstHitReceivedFromTeamIndex() {
		if (_playerState != PlayerState.Out && _receivedHitFromTeamMates.size() > 0) {
			List<WBPlayer> teamHit = _receivedHitFromTeamMates.get(0);
			return _team.getIndexOfTeamHitReceived(teamHit);
		}
		return 0;
	}

	public int getSecondHitReceivedFromTeamIndex() {
		if (_playerState != PlayerState.Out && _receivedHitFromTeamMates.size() > 1) {
			List<WBPlayer> teamHit = _receivedHitFromTeamMates.get(1);
			return _team.getIndexOfTeamHitReceived(teamHit);
		}
		return 0;
	}

	public String getFirstHitReceivedFromTeamString() {
		if (_playerState != PlayerState.Out && _receivedHitFromTeamMates.size() > 0) {
			List<WBPlayer> teamHit = _receivedHitFromTeamMates.get(0);
			return _team.getStringOfHitReceivedFromTeam(teamHit);
		}
		return "";
	}

	public String getSecondHitReceivedFromTeamString() {
		if (_playerState != PlayerState.Out && _receivedHitFromTeamMates.size() > 1) {
			List<WBPlayer> teamHit = _receivedHitFromTeamMates.get(1);
			return _team.getStringOfHitReceivedFromTeam(teamHit);
		}
		return "";
	}

	public void addHitReceivedFromOpponent(WBPlayer player) {
		if (_playerState == PlayerState.Out)
			return;
		
		setPlayerState(PlayerState.fromInt(getPlayerState().toInt() + 1));
		_receivedHitFromOpponents.add(player);
	}

	public void removeHitReceivedFromOutOpponentAtIndex(int index) {
		if (getPlayerState() == PlayerState.Out || getPlayerState() == PlayerState.Green)
			return;
		
		setPlayerState(PlayerState.fromInt(getPlayerState().toInt() - 1));

		int opponentNumber = (index == 0) ? getPlayerNumberOfFirstHitReceivedFromOpponent()
				: getPlayerNumberOfSecondHitReceivedFromOpponent();

		WBPlayer opponent = _team.getOpponent().getPlayerAtNumber(opponentNumber);

		int removedIndex = opponent.getHitOpponents().indexOf(this);
		if (index == 1)
			removedIndex = opponent.getHitOpponents().lastIndexOf(this);

		opponent.getHitOpponents().remove(removedIndex);
		opponent.getHitOpponentIndexes().remove(removedIndex);
		
		_receivedHitFromOpponents.remove(index);
	}

	public boolean removeHitReceivedFromOpponent(WBPlayer opponent) {
		if (_playerState == PlayerState.Out || _playerState == PlayerState.Green)
			return false;
		
		int index = opponent.getHitOpponents().indexOf(this);
		if (index == 0) {
			opponent.getHitOpponents().remove(0);
			opponent.getHitOpponentIndexes().remove(0);

			int findIndex = _receivedHitFromOpponents.indexOf(opponent);
			_receivedHitFromOpponents.remove(findIndex);

			setPlayerState(PlayerState.fromInt(getPlayerState().toInt() - 1));
			return true;
		}
		return false;
	}

	public int getPlayerNumberOfFirstHitReceivedFromOpponent() {
		if (_playerState != PlayerState.Out && _receivedHitFromOpponents.size() > 0) {
			WBPlayer opponent = _receivedHitFromOpponents.get(0);
			return opponent.getPlayerNumber();
		}
		return 0;
	}

	public int getPlayerNumberOfSecondHitReceivedFromOpponent() {
		if (_playerState != PlayerState.Out && _receivedHitFromOpponents.size() > 1) {
			WBPlayer opponent = _receivedHitFromOpponents.get(1);
			return opponent.getPlayerNumber();
		}
		return 0;
	}

	public int getFirstHitReceivedFromOpponentIndex() {
		if (_playerState != PlayerState.Out && _receivedHitFromOpponents.size() > 0) {
			WBPlayer opponent = _receivedHitFromOpponents.get(0);
			int index = opponent.getHitOpponents().indexOf(this);
			return opponent.getHitOpponentIndexes().get(index);
		}
		return 0;
	}

	public int getSecondHitReceivedFromOpponentIndex() {
		if (_playerState != PlayerState.Out && _receivedHitFromOpponents.size() > 1) {
			int index = 0;
			WBPlayer opponent = _receivedHitFromOpponents.get(1);

			if (getPlayerNumberOfFirstHitReceivedFromOpponent() == getPlayerNumberOfSecondHitReceivedFromOpponent()) {
				int secondIndex = opponent.getHitOpponents().lastIndexOf(this);
				index = secondIndex;
			} else
				index = opponent.getHitOpponents().indexOf(this) ;
			
			return opponent.getHitOpponentIndexes().get(index);
		}
		return 0;
	}

	public List<WBPlayer> getHitOpponents() {
		return _hitOpponents;
	}

	public List<Integer> getHitOpponentIndexes() {
		return _hitIndexes;
	}
	
	public void setPlayerState(PlayerState playerState) {
		_oldState = _playerState;
		_playerState = playerState;
	}

	public void saveState() {
		_oldState = _playerState;
	}
	
	public PlayerState getPlayerState() {
		return _playerState;
	}

	public String getTextOfState(PlayerState state) {
		//TODO: LOCALIZE
		
		if (state == PlayerState.Green)
			return "Green";
		else if (state == PlayerState.Yellow)
			return "Yellow";
		else if (state == PlayerState.Red)
			return "Red";
		else if (state == PlayerState.Out)
			return "Out";
		else
			return "";
	}

	public void setSetupState(SetupState setupState) {
		_setupState = setupState;
	}

	public SetupState getSetupState() {
		return _setupState;
	}

	public void setPlayerNumber(int playerNumber) {
		_playerNumber = playerNumber;
	}

	public int getPlayerNumber() {
		return _playerNumber;
	}

	public PlayerRole getRole() {
		return _role;
	}

	public void setRole(PlayerRole role) {
		_role = role;
	}

	public WBTeam getTeam() {
		return _team;
	}

	public void setIndivCount(int count) {
		_indivCount = count;
	}

	public int getIndivCount() {
		return _indivCount;
	}
	
	public String getStateChangedText() {
	    if (_oldState != _playerState)
	        return getTextOfState(_oldState) + " -> " + getTextOfState(_playerState);
	    else
	        return "";
	}
}
