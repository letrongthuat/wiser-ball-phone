package com.goodapps.wiserball;

import com.goodapps.wiserball.type.TeamColor;

import android.app.Fragment;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;

public class StatiticsFragment extends Fragment {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_statitics, container, false);

		((TextView) v.findViewById(R.id.red_statitics).findViewById(
				R.id.text_title_team)).setText(R.string.label_red_team);
		((TextView) v.findViewById(R.id.white_statitics).findViewById(
				R.id.text_title_team)).setText(R.string.label_white_team);

		return v;
	}

	public void addRow(int parentResource, String name, int total, int hit, int miss, int skip, int fault) {
		View parent = getView().findViewById(parentResource);
		TableLayout table = (TableLayout) parent.findViewById(R.id.table_statitics);

		TableRow row = new TableRow(getActivity());
		table.addView(row);

		TextView textName = new TextView(getActivity());
		textName.setText(name);
		row.addView(textName);
		LayoutParams nameParams = (LayoutParams) textName.getLayoutParams();
		nameParams.weight = 4;

		TextView textTotal = new TextView(getActivity());
		textTotal.setText(String.valueOf(total));
		textTotal.setGravity(Gravity.CENTER);
		row.addView(textTotal);
		LayoutParams totalParams = (LayoutParams) textTotal.getLayoutParams();
		totalParams.weight = 1;
		textTotal.setLayoutParams(totalParams);
		
		TextView textHit = new TextView(getActivity());
		textHit.setText(String.valueOf(hit));
		textHit.setGravity(Gravity.CENTER);
		row.addView(textHit);
		LayoutParams hitParams = (LayoutParams) textHit.getLayoutParams();
		hitParams.weight = 1;
		textHit.setLayoutParams(hitParams);

		TextView textMiss = new TextView(getActivity());
		textMiss.setText(String.valueOf(miss));
		textMiss.setGravity(Gravity.CENTER);
		row.addView(textMiss);
		LayoutParams missParams = (LayoutParams) textMiss.getLayoutParams();
		missParams.weight = 1;
		textMiss.setLayoutParams(missParams);

		TextView textSkip = new TextView(getActivity());
		textSkip.setText(String.valueOf(skip));
		textSkip.setGravity(Gravity.CENTER);
		row.addView(textSkip);
		LayoutParams skipParams = (LayoutParams) textSkip.getLayoutParams();
		skipParams.weight = 1;
		textSkip.setLayoutParams(skipParams);

		TextView textFault = new TextView(getActivity());
		textFault.setText(String.valueOf(fault));
		textFault.setGravity(Gravity.CENTER);
		row.addView(textFault);
		LayoutParams faultParams = (LayoutParams) textSkip.getLayoutParams();
		faultParams.weight = 1;
		textFault.setLayoutParams(faultParams);
	}

	public void removeAllRows(int parentResource) {
		View parent = getView().findViewById(parentResource);
		TableLayout table = (TableLayout) parent.findViewById(R.id.table_statitics);

		table.removeViews(1, table.getChildCount() - 1);
	}

	public void setRedTeamName(String name) {
		TextView tv = (TextView) getActivity().findViewById(R.id.red_statitics).findViewById(
				R.id.text_title_team);
		tv.setText(name);
	}
	
	public void setRedTurns(int turns) {
		TextView tv = (TextView) getActivity().findViewById(R.id.red_statitics).findViewById(R.id.text_turns);
		tv.setText(String.valueOf(turns));
	}
	
	public void setRedTeamFault(int teamFault) {
		TextView tv = (TextView) getActivity().findViewById(R.id.red_statitics).findViewById(R.id.text_team_fault);
		tv.setText(String.valueOf(teamFault));
	}

	public void setWhiteTeamName(String name) {
		TextView tv = (TextView) getActivity().findViewById(R.id.white_statitics).findViewById(
				R.id.text_title_team);
		tv.setText(name);
	}
	
	public void setWhiteTurns(int turns) {
		TextView tv = (TextView) getActivity().findViewById(R.id.white_statitics).findViewById(R.id.text_turns);
		tv.setText(String.valueOf(turns));
	}
	
	public void setWhiteTeamFault(int teamFault) {
		TextView tv = (TextView) getActivity().findViewById(R.id.white_statitics).findViewById(R.id.text_team_fault);
		tv.setText(String.valueOf(teamFault));
	}
	
	public void setTeamWin(TeamColor color) {
		TextView redTv = (TextView) getActivity().findViewById(R.id.red_statitics).findViewById(R.id.text_result);
		TextView whiteTv = (TextView) getActivity().findViewById(R.id.white_statitics).findViewById(R.id.text_result);
		
		if (color == TeamColor.Red) {
			redTv.setText(getString(R.string.label_win));
			whiteTv.setText(getString(R.string.label_lose));
		} else {
			redTv.setText(getString(R.string.label_lose));
			whiteTv.setText(getString(R.string.label_win));
		}
	}
}
