package com.goodapps.wiserball;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.text.InputType;
import android.widget.EditText;

import com.goodapps.wiserball.type.SetupState;
import com.goodapps.wiserball.type.TeamColor;

public class DialogFactory {
	public static void showGameOverAlert(Context context, OnClickListener onOkListener) {
		AlertDialog alert = new AlertDialog.Builder(context).setTitle(R.string.title_game_over)
				.setMessage(R.string.message_game_over)
				.setPositiveButton(R.string.title_ok, onOkListener).create();
		alert.show();
	}

	public static void showInvalidMoveAlert(Context context, OnClickListener onOkListener) {
		AlertDialog alert = new AlertDialog.Builder(context).setTitle(R.string.title_notice)
				.setMessage(R.string.message_invalid_move)
				.setPositiveButton(R.string.title_ok, onOkListener).create();
		alert.show();
	}

	public static void showInvalidSetupAlert(Context context, SetupState setupState,
			OnClickListener onOkListener) {
		AlertDialog alert = new AlertDialog.Builder(context)
				.setTitle(R.string.title_notice)
				.setMessage(
						context.getString(
								(setupState == SetupState.Attack ? R.string.message_invalid_attackers_number
										: R.string.message_invalid_defenders_number)))
				.setPositiveButton(R.string.title_ok, onOkListener).create();
		alert.show();
	}

	public static void showInvalidSideAlert(Context context, OnClickListener onOkListener) {
		AlertDialog alert = new AlertDialog.Builder(context).setTitle(R.string.title_notice)
				.setMessage(R.string.message_wrong_side)
				.setPositiveButton(R.string.title_ok, onOkListener).create();
		alert.show();
	}

	public static void showInvalidPlayerStateAlert(Context context, OnClickListener onOkListener) {
		AlertDialog alert = new AlertDialog.Builder(context).setTitle(R.string.title_notice)
				.setMessage(R.string.message_invalid_player_state)
				.setPositiveButton(R.string.title_ok, onOkListener).create();
		alert.show();
	}

	public static void showCantRemoveStaterAlert(Context context, OnClickListener onOkListener) {
		AlertDialog alert = new AlertDialog.Builder(context).setTitle(R.string.title_notice)
				.setMessage(R.string.message_clear_hitchain)
				.setPositiveButton(R.string.title_ok, onOkListener).create();
		alert.show();
	}

	public static void showLuckyTeamAlert(Context context, TeamColor luckyTeam,
			OnClickListener onOkListener) {
		AlertDialog alert = new AlertDialog.Builder(context)
				.setTitle(R.string.title_lucky_team)
				.setMessage(
						context.getString(R.string.message_lucky_team, context
								.getString(luckyTeam == TeamColor.Red ? R.string.label_red_name
										: R.string.label_white_name)))
				.setPositiveButton(R.string.title_ok, onOkListener).create();
		alert.show();
	}

	public static void showWinnerAlert(Context context, TeamColor winnerTeam,
			OnClickListener onPositiveListener, OnClickListener onNegativeListener) {
		AlertDialog alert = new AlertDialog.Builder(context)
				.setTitle(R.string.title_game_over)
				.setMessage(
						context.getString(R.string.message_game_winner, context
								.getString(winnerTeam == TeamColor.Red ? R.string.label_red_name
										: R.string.label_white_name)))
				.setPositiveButton(R.string.title_yes, onPositiveListener)
				.setNegativeButton(R.string.title_no, onNegativeListener).create();
		alert.show();
	}

	public static AlertDialog getInputTextDialog(Context context, int titleResourceId, String hintText,
			OnClickListener onOkListener, OnClickListener onCancelListener) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context).setTitle(titleResourceId);
		// Set up the input
		final EditText input = new EditText(context);
		// Specify the type of input expected; this, for example, sets the input
		// as a password, and will mask the text
		input.setInputType(InputType.TYPE_CLASS_TEXT);
		input.setId(R.id.edit_name);
		input.setHint(hintText);
		builder.setView(input);

		// Set up the buttons
		builder.setPositiveButton(R.string.title_ok, onOkListener);
		builder.setNegativeButton(R.string.title_cancel, onCancelListener);

		return builder.create();
	}

	public static AlertDialog getQuestionDialog(Context context, String title,
			String message, OnClickListener onYesListener, OnClickListener onNoListener) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context).setTitle(title)
				.setMessage(message);

		// Set up the buttons
		builder.setPositiveButton(R.string.title_yes, onYesListener);
		builder.setNegativeButton(R.string.title_no, onNoListener);

		return builder.create();
	}

	public static void showWrongFileName(Context context, String fileName, OnClickListener onOkListener) {
		AlertDialog alert = new AlertDialog.Builder(context).setTitle(R.string.title_notice)
				.setMessage(R.string.message_duplicate_filename)
				.setPositiveButton(R.string.title_ok, onOkListener).create();
		alert.show();
	}
	
	public static void showInfoDialog(Context context, int versionCode) {
		AlertDialog alert = new AlertDialog.Builder(context).setTitle(R.string.title_about)
				.setMessage("Wiser Ball 1.0 #" + versionCode)
				.setPositiveButton(R.string.title_ok, null).create();
		alert.show();
	}
	
	public static void showMessageDialog(Context context, String message) {
		AlertDialog alert = new AlertDialog.Builder(context).setTitle(R.string.title_notice)
				.setMessage(message).create();
		alert.show();
	}
}
