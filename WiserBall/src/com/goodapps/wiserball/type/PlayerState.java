package com.goodapps.wiserball.type;

public enum PlayerState {
	Green,
	Yellow,
	Red,
	Out;
	
	public int toInt() {
		switch (this) 
		{
		case Green:
			return 0;
		case Yellow:
			return 1;
		case Red:
			return 2;
		case Out:
			return 3;
		}
		return 0;
	}
	
	public static PlayerState fromInt(int value) {
		switch (value) {
		case 0:
			return Green;
		case 1:
			return Yellow;
		case 2:
			return Red;
		case 3:
			return Out;
		}
		
		return Green;
	}
}
