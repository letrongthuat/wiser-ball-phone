package com.goodapps.wiserball;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.goodapps.wiserball.MultiDirectionSlidingDrawerBottom.OnDrawerCloseListenerBottom;
import com.goodapps.wiserball.MultiDirectionSlidingDrawerBottom.OnDrawerOpenListenerBottom;
import com.goodapps.wiserball.MultiDirectionSlidingDrawerLeft.OnDrawerCloseListenerLeft;
import com.goodapps.wiserball.MultiDirectionSlidingDrawerLeft.OnDrawerOpenListenerLeft;
import com.goodapps.wiserball.MultiDirectionSlidingDrawerTop.OnDrawerCloseListener;
import com.goodapps.wiserball.MultiDirectionSlidingDrawerTop.OnDrawerOpenListener;
import com.goodapps.wiserball.data.DbHelper;
import com.goodapps.wiserball.data.PlayerContract;
import com.goodapps.wiserball.data.TeamContract;
import com.goodapps.wiserball.game.WBMatch;
import com.goodapps.wiserball.game.WBPlayer;
import com.goodapps.wiserball.game.WBTeam;
import com.goodapps.wiserball.game.WBTurn;
import com.goodapps.wiserball.type.GameState;
import com.goodapps.wiserball.type.PlayerRole;
import com.goodapps.wiserball.type.PlayerState;
import com.goodapps.wiserball.type.SetupState;
import com.goodapps.wiserball.type.TeamColor;
import com.goodapps.wiserball.type.TurnType;
import com.goodapps.wiserball.widget.PlayerView;



public class MainActivity extends Activity implements OnItemSelectedListener {

	private static final int DEFAULT_TIME_STEP = 10;
	private static final int MIN_COUNT = 10;
	private int _ballNumber;
	private boolean _isWhiteFirst;
	private GameState _gameState;
	private PlayerView _redPlayerViews[];
	private PlayerView _whitePlayerViews[];
	private WBMatch _match;
	private List<WBPlayer> _hitChain;
	private SettingController _settings;
	private int _count;
	private long _duration;
	private DbHelper _dbHelper;
	private boolean _isLess;

	public static final String EXTRA_SELECT_FILE = "select_file";
	public static final int REQUEST_CODE_SELECT = 100;
	MultiDirectionSlidingDrawerTop mDrawer;
	MultiDirectionSlidingDrawerBottom mDrawer_1;
	MultiDirectionSlidingDrawerLeft mDrawer_2;;
	ImageView imageTop,imageBottom;
	

	@SuppressLint("HandlerLeak")
	private Handler _countDownHandler = new Handler() {
		@Override
		public void handleMessage(android.os.Message msg) {
			if (_count > 0) {
				_count--;
				((TextView) findViewById(R.id.text_count)).setText(String.valueOf(_count));
			}

			if (_count == 0) {
				// TODO: ask for change turn
				onChangeTurnClicked(null);
			} else {
				_countDownHandler.sendEmptyMessageDelayed(0, 1000);
			}
		};
	};

	@SuppressLint("SimpleDateFormat")
	private TimerTask _timerTask = new TimerTask() {
		@Override
		public void run() {
			MainActivity.this.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					TextView textTime = (TextView) findViewById(R.id.text_time);
					SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss");
					textTime.setText(format.format(new Date()));

					if (_gameState == GameState.Playing || _gameState == GameState.Setupping) {
						_duration++;

						int ss = (int) (_duration % 60);
						int mm = (int) ((_duration / 60) % 60);
						int hh = (int) (_duration / 3600);

						TextView textMatchTime = (TextView) findViewById(R.id.text_match_time);
						textMatchTime.setText(String.format("%02d:%02d:%02d", hh, mm, ss));
					}
				}
			});
		}
	};

	private OnItemSelectedListener _onRedTeamSelectedListener = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
			if (id != -1) {
				List<PlayerContract> players = _dbHelper.getAllPlayersForTeam(id);
				for (int i = 0; i < 7; i++) {
					int playerNumber = _redPlayerViews[i].getPlayerNumber();
					if (playerNumber <= players.size() && playerNumber > 0) {
						_redPlayerViews[i].setPlayerName(players.get(playerNumber - 1).name);
						_redPlayerViews[i]
								.setRole(players.get(playerNumber - 1).role == 1 ? PlayerRole.Captain
										: PlayerRole.Normal);
					} else {
						_redPlayerViews[i].setPlayerName(getString(R.string.default_player_name));
						_redPlayerViews[i].setRole(PlayerRole.Normal);
					}
				}
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> adapterView) {

		}

	};

	private OnItemSelectedListener _onWhiteTeamSelectedListener = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
			if (id != -1) {
				List<PlayerContract> players = _dbHelper.getAllPlayersForTeam(id);
				for (int i = 0; i < 7; i++) {
					int playerNumber = _whitePlayerViews[i].getPlayerNumber();
					if (playerNumber <= players.size() && playerNumber > 0) {
						_whitePlayerViews[i].setPlayerName(players.get(playerNumber - 1).name);
						_whitePlayerViews[i]
							.setRole(players.get(playerNumber - 1).role == 1 ? PlayerRole.Captain
									: PlayerRole.Normal);
					}
					else {
						_whitePlayerViews[i].setPlayerName(getString(R.string.default_player_name));
						_whitePlayerViews[i].setRole(PlayerRole.Normal);
					}
				}
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> adapterView) {

		}

	};

	private int _versionCode;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		_isLess = false;
		try {
			_versionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
			setTitle(getString(R.string.app_name) + " - #"
					+ _versionCode);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		setContentView(R.layout.activity_main_phone);
//		LinearLayout test = (LinearLayout)findViewById(R.id.coverRed);
//		test.setBackgroundColor(Color.TRANSPARENT);
		Button lang = (Button) findViewById(R.id.button_language);
		lang.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onLanguageClicked(v);
			}
		});
		 //add left slide menu
//				slidingMenu_left = new SlidingMenu(this);
//				slidingMenu_left.setMode(SlidingMenu.LEFT);
//				// nếu là TOUCHMODE_FULLSCREEN sẽ ko click dc trong menu
//				slidingMenu_left.setTouchModeBehind(SlidingMenu.TOUCHMODE_MARGIN);
//				
//				// slidingMenu.setShadowDrawable(R.drawable.actionbar_gradient);
//				slidingMenu_left.setShadowWidth(30);
//				slidingMenu_left.setFadeDegree(0.0f);
//				slidingMenu_left.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
//				slidingMenu_left.setBehindWidth(450);
//				slidingMenu_left.setMenu(R.layout.slide_left);
		
		_settings = new SettingController(this);
		_dbHelper = new DbHelper(this);
		// set last ball number
		Spinner spinner = (Spinner) findViewById(R.id.spinner_ball_number);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
				R.array.ball_number, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter);
		spinner.setOnItemSelectedListener(this);
		spinner.setSelection(_settings.getBallCount() - 5);

		// set first team
		ToggleButton toggleWhiteFirst = (ToggleButton) findViewById(R.id.toggle_white_first);
		toggleWhiteFirst.setChecked(_settings.isWhiteFirst());
		toggleWhiteFirst.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				_isWhiteFirst = isChecked;
				_settings.setWhiteFirst(isChecked);
			}
		});

		_redPlayerViews = new PlayerView[7];
		_whitePlayerViews = new PlayerView[7];
		_hitChain = new LinkedList<WBPlayer>();

		View redTeam = findViewById(R.id.layout_red_team);
		_redPlayerViews[0] = (PlayerView) redTeam.findViewById(R.id.player_1);
		_redPlayerViews[1] = (PlayerView) redTeam.findViewById(R.id.player_2);
		_redPlayerViews[2] = (PlayerView) redTeam.findViewById(R.id.player_3);
		_redPlayerViews[3] = (PlayerView) redTeam.findViewById(R.id.player_4);
		_redPlayerViews[4] = (PlayerView) redTeam.findViewById(R.id.player_5);
		_redPlayerViews[5] = (PlayerView) redTeam.findViewById(R.id.player_6);
		_redPlayerViews[6] = (PlayerView) redTeam.findViewById(R.id.player_7);

		View whiteTeam = findViewById(R.id.layout_white_team);
		_whitePlayerViews[0] = (PlayerView) whiteTeam.findViewById(R.id.player_1);
		_whitePlayerViews[1] = (PlayerView) whiteTeam.findViewById(R.id.player_2);
		_whitePlayerViews[2] = (PlayerView) whiteTeam.findViewById(R.id.player_3);
		_whitePlayerViews[3] = (PlayerView) whiteTeam.findViewById(R.id.player_4);
		_whitePlayerViews[4] = (PlayerView) whiteTeam.findViewById(R.id.player_5);
		_whitePlayerViews[5] = (PlayerView) whiteTeam.findViewById(R.id.player_6);
		_whitePlayerViews[6] = (PlayerView) whiteTeam.findViewById(R.id.player_7);

		for (int i = 0; i < 7; i++) {
			_redPlayerViews[i].setOnPlayerClickListener(_onPlayerViewClickListener);
			_redPlayerViews[i].setOnSetupSelectListener(this);
			_whitePlayerViews[i].setOnPlayerClickListener(_onPlayerViewClickListener);
			_whitePlayerViews[i].setTeamColor(TeamColor.White);
			_whitePlayerViews[i].setOnSetupSelectListener(this);
		}

		// reset view
		setGameState(GameState.Closed);

		// timer
		new Timer().scheduleAtFixedRate(_timerTask, 0, 1000);
		
		setLess(_isLess);
		updateLanguage(Utils.getLocale(_settings.getLanguage()));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		Resources res = Utils.getResourcesForLocale(getResources(), Utils.getLocale(_settings.getLanguage()));
		menu.getItem(0).setTitle(res.getString(R.string.item_teams));
		menu.getItem(1).setTitle(res.getString(R.string.item_files));
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.item_team:
			Intent teamIntent = new Intent(this, TeamActivity.class);
			startActivity(teamIntent);
			break;
		case R.id.item_file:
			Intent fileIntent = new Intent(this, FileActivity.class);
			startActivity(fileIntent);
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private OnClickListener _onPlayerViewClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (_gameState != GameState.Playing)
				return;

			PlayerView playerView = (PlayerView) v.getTag();

			if (_hitChain.size() == 0 && !playerView.isSelected()) {
				DialogFactory.showInvalidSideAlert(MainActivity.this, null);
				return;
			}

			WBPlayer player = (WBPlayer) playerView.getTag();

			if (player.getPlayerState() == PlayerState.Out)
				return;

			if (player.getPlayerState() != PlayerState.Green && _hitChain.size() == 0) {
				DialogFactory.showInvalidPlayerStateAlert(MainActivity.this, null);
				return;
			}

			playClickBallSound();
			boolean selected = !v.isSelected();

			if (selected) {
				_hitChain.add(player);
			} else {
				if (_hitChain.indexOf(player) == 0 && _hitChain.size() > 1) {
					DialogFactory.showCantRemoveStaterAlert(MainActivity.this, null);
					return;
				}
				_hitChain.remove(player);
			}

			playerView.getPlayerButton().setSelected(selected);
		}
	};

	private void setupPlayerViewsForBallCount(int ballCount) {
		PlayerView playerViews[][] = new PlayerView[2][];
		playerViews[0] = _redPlayerViews;
		playerViews[1] = _whitePlayerViews;

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < ballCount; j++) {
				playerViews[i][j].setVisibility(View.VISIBLE);
			}

			for (int j = ballCount; j < 7; j++) {
				playerViews[i][j].setVisibility(View.GONE);
			}

			playerViews[i][(_ballNumber - 1) / 2].setPlayerNumber(1);

			for (int j = 0; j < (_ballNumber - 1) / 2; j++) {
				playerViews[i][j].setPlayerNumber(j + 2);
			}

			for (int j = (_ballNumber + 1) / 2; j < _ballNumber; j++) {
				playerViews[i][j].setPlayerNumber(j + 1);
			}
		}
	}

	private void resetViewsBeforeMatch() {
		PlayerView playerViews[][] = new PlayerView[2][];
		playerViews[0] = _redPlayerViews;
		playerViews[1] = _whitePlayerViews;

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 7; j++) {
				PlayerView playerView = playerViews[i][j];
				playerView.setSelected(false);
				playerView.setPlayerState(PlayerState.Green);
				playerView.setIndiv(0);
				playerView.setHit1Hidden(true);
				playerView.setHit2Hidden(true);
				playerView.setFirstHitReceivedFromTeamString("");
				playerView.setSecondHitReceivedFromTeamString("");
				playerView.getPlayerButton().setEnabled(false);
				playerView.setSetupVisible(false);
				playerView.setSetupEnabled(false);
				playerView.setSetupState(SetupState.Undefined);
				playerView.setRole(PlayerRole.Normal);
				playerView.setPlayerName(getString(R.string.default_player_name));
			}
		}

		((TextView) findViewById(R.id.text_red_fault_turn)).setText(R.string.zero);
		((TextView) findViewById(R.id.text_red_captute_zone)).setText(R.string.zero);
		((TextView) findViewById(R.id.text_white_fault_turn)).setText(R.string.zero);
		((TextView) findViewById(R.id.text_white_captute_zone)).setText(R.string.zero);

		findViewById(R.id.button_red_fault_turn_minus).setEnabled(false);
		findViewById(R.id.button_red_captute_zone_minus).setEnabled(false);
		findViewById(R.id.button_white_fault_turn_minus).setEnabled(false);
		findViewById(R.id.button_white_captute_zone_minus).setEnabled(false);

		((TextView) findViewById(R.id.text_current_turn)).setText(R.string.default_turn);
		((TextView) findViewById(R.id.text_count))
				.setText(String.valueOf(_settings.getCountDown()));
		((TextView) findViewById(R.id.text_match_time)).setText(R.string.default_time);
		findViewById(R.id.button_time_minus).setEnabled(_settings.getCountDown() > MIN_COUNT);

		// adjust red st & stf
//		View redTeamLayout = findViewById(R.id.layout_red_team);
//		((TextView) redTeamLayout.findViewById(R.id.text_skip_turn)).setText(R.string.zero);
//		((TextView) redTeamLayout.findViewById(R.id.text_skip_turn_fault)).setText(R.string.zero);

		View redTeamLayout = findViewById(R.id.slide_left_red);
		((TextView) redTeamLayout.findViewById(R.id.text_skip_turn)).setText(R.string.zero);
		((TextView) redTeamLayout.findViewById(R.id.text_skip_turn_fault)).setText(R.string.zero);
		// adjust white st & stf
//		View whiteTeamLayout = findViewById(R.id.layout_white_team);
//		((TextView) whiteTeamLayout.findViewById(R.id.text_skip_turn)).setText(R.string.zero);
//		((TextView) whiteTeamLayout.findViewById(R.id.text_skip_turn_fault)).setText(R.string.zero);
		
		View whiteTeamLayout = findViewById(R.id.slide_left_white);
		((TextView) whiteTeamLayout.findViewById(R.id.text_skip_turn)).setText(R.string.zero);
		((TextView) whiteTeamLayout.findViewById(R.id.text_skip_turn_fault)).setText(R.string.zero);

		TextView textRedBallRescued = (TextView) findViewById(R.id.text_red_ball_rescued);
		textRedBallRescued.setText("");

		TextView textWhiteBallRescued = (TextView) findViewById(R.id.text_white_ball_rescued);
		textWhiteBallRescued.setText("");

		TextView textRedBall = (TextView) findViewById(R.id.text_red_ball);
		textRedBall.setText("");
		TextView textWhiteBall = (TextView) findViewById(R.id.text_white_ball);
		textWhiteBall.setText("");
		
		loadTeamListForSpinner((Spinner) redTeamLayout.findViewById(R.id.spinner_team),
				_onRedTeamSelectedListener);

		loadTeamListForSpinner((Spinner) whiteTeamLayout.findViewById(R.id.spinner_team),
				_onWhiteTeamSelectedListener);
	}

	private void updateViewsAfterTurn() {
		PlayerView playerViews[][] = new PlayerView[2][];
		playerViews[0] = _redPlayerViews;
		playerViews[1] = _whitePlayerViews;

		if (_match.getCurrentTurnIndex() < _ballNumber * 2)
			setGameState(GameState.Setupping);
		else
			setGameState(GameState.Playing);

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < _ballNumber; j++) {
				PlayerView playerView = playerViews[i][j];

				playerView.setSelected(i == 0 ? !isWhiteTurn() : isWhiteTurn());
				playerView.getPlayerButton().setSelected(false);
				playerView.setControlEnabled(_gameState == GameState.Playing);
				playerView.setSetupVisible(_gameState == GameState.Setupping);

				WBPlayer player = (WBPlayer) playerView.getTag();
				playerView.setSetupEnabled(playerView.isSelected()
						&& playerView.getSetupState() == SetupState.Undefined);

				playerView.setSetupState(player.getSetupState());
				playerView.setPlayerState(player.getPlayerState());
				playerView.setFirstHitReceivedFromTeamString(player
						.getFirstHitReceivedFromTeamString());
				playerView.setSecondHitReceivedFromTeamString(player
						.getSecondHitReceivedFromTeamString());
				playerView.setPlayerNumberOfFirstHitFromOpponent(player
						.getPlayerNumberOfFirstHitReceivedFromOpponent());
				playerView.setPlayerNumberOfSecondHitFromOpponent(player
						.getPlayerNumberOfSecondHitReceivedFromOpponent());
				playerView.setFirstHitReceivedFromOpponentIndex(player
						.getFirstHitReceivedFromOpponentIndex());
				playerView.setSecondHitReceivedFromOpponentIndex(player
						.getSecondHitReceivedFromOpponentIndex());
				playerView.setIndiv(player.getIndivCount());
				playerView.setRole(player.getRole());
			}
		}

		// adjust red st & stf
		//View redTeamLayout = findViewById(R.id.layout_red_team);
		View redTeamLayout = findViewById(R.id.slide_left_red);
		TextView redST = (TextView) redTeamLayout.findViewById(R.id.text_skip_turn);
		redST.setText(String.valueOf(_match.getRedTeam().getSkipTurn()));
		TextView redSTF = (TextView) redTeamLayout.findViewById(R.id.text_skip_turn_fault);
		redSTF.setText(String.valueOf(_match.getRedTeam().getSkipTurnFault()));

		// adjust white st & stf
		//View whiteTeamLayout = findViewById(R.id.layout_white_team);
		View whiteTeamLayout = findViewById(R.id.slide_left_white);
		TextView whiteST = (TextView) whiteTeamLayout.findViewById(R.id.text_skip_turn);
		whiteST.setText(String.valueOf(_match.getWhiteTeam().getSkipTurn()));
		TextView whiteSTF = (TextView) whiteTeamLayout.findViewById(R.id.text_skip_turn_fault);
		whiteSTF.setText(String.valueOf(_match.getWhiteTeam().getSkipTurnFault()));

		TextView textTurn = (TextView) findViewById(R.id.text_current_turn);

		textTurn.setText(String.format("%d / %d", _match.getCurrentTurnIndex(),
				_match.getTurnCount()));

		findViewById(R.id.button_back).setEnabled(_match.canBack());
		findViewById(R.id.button_next).setEnabled(_match.canNext());

		TextView textRedBallRescued = (TextView) findViewById(R.id.text_red_ball_rescued);
		textRedBallRescued.setText(_match.getBallRescuedText(_match.getRedTeam()));

		TextView textWhiteBallRescued = (TextView) findViewById(R.id.text_white_ball_rescued);
		textWhiteBallRescued.setText(_match.getBallRescuedText(_match.getWhiteTeam()));

		if (_match.getLastTeamColor() == TeamColor.Red) {
			TextView textRedBall = (TextView) findViewById(R.id.text_red_ball);
			textRedBall.setText(_match.getDescriptionOfTurn(_match.getLastTurn()));
			TextView textWhiteBall = (TextView) findViewById(R.id.text_white_ball);
			textWhiteBall.setText("");
		} else {
			TextView textRedBall = (TextView) findViewById(R.id.text_red_ball);
			textRedBall.setText("");
			TextView textWhiteBall = (TextView) findViewById(R.id.text_white_ball);
			textWhiteBall.setText(_match.getDescriptionOfTurn(_match.getLastTurn()));
		}
	}

	private boolean isWhiteTurn() {
		if (_match != null && _match.isWhiteTurn())
			return true;
		return false;
	}

	/*
	 * Game Controls
	 */

	public void onNewGameClicked(View view) {
		setGameState(GameState.Setupping);
		_duration = 0;
		_match = new WBMatch(_ballNumber, _isWhiteFirst);
		_hitChain.clear();

		// binding player & player views
		PlayerView playerViews[][] = new PlayerView[2][];
		playerViews[0] = _redPlayerViews;
		playerViews[1] = _whitePlayerViews;
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < _ballNumber; j++) {
				PlayerView playerView = playerViews[i][j];
				WBTeam team = (i == 0) ? _match.getRedTeam() : _match.getWhiteTeam();
				WBPlayer player = team.getPlayerAtNumber(playerView.getPlayerNumber());
				playerView.setTag(player);
			}
		}

		// update player views
		updateViewsAfterTurn();
	}

	public void onOpenGameClicked(View view) {
		Intent i = new Intent(this, FileActivity.class);
		i.putExtra(EXTRA_SELECT_FILE, true);

		startActivityForResult(i, REQUEST_CODE_SELECT);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == REQUEST_CODE_SELECT && resultCode == RESULT_OK) {
			try {
				_match = WBMatch.openFromFile(data.getStringExtra(EXTRA_SELECT_FILE));
				setGameState(GameState.Opening);
				setupPlayerViewsForBallCount(_match.getNumberOfPlayers());
				_hitChain.clear();
				
				//TODO:
				((ToggleButton) findViewById(R.id.toggle_white_first)).setChecked(_match.isWhiteFirst());
				_isWhiteFirst = _match.isWhiteFirst();
				
				((TextView) findViewById(R.id.text_red_fault_turn)).setText(String.valueOf(_match.getRedTeam().getTeamFault()));
				((TextView) findViewById(R.id.text_white_fault_turn)).setText(String.valueOf(_match.getWhiteTeam().getTeamFault()));
				
				ArrayList<String> red = new ArrayList<String>();
				red.add(_match.getRedTeam().getName());
				ArrayAdapter<String> redAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, android.R.id.text1, red);
				//((Spinner) findViewById(R.id.layout_red_team).findViewById(R.id.spinner_team)).setAdapter(redAdapter);
				//((Spinner) findViewById(R.id.layout_red_team).findViewById(R.id.spinner_team)).setOnItemSelectedListener(null);
				((Spinner) findViewById(R.id.slide_left_red).findViewById(R.id.spinner_team)).setAdapter(redAdapter);
				((Spinner) findViewById(R.id.slide_left_red).findViewById(R.id.spinner_team)).setOnItemSelectedListener(null);
				
				ArrayList<String> white = new ArrayList<String>();
				white.add(_match.getWhiteTeam().getName());
				ArrayAdapter<String> whiteAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, android.R.id.text1, white);
				//((Spinner) findViewById(R.id.layout_white_team).findViewById(R.id.spinner_team)).setAdapter(whiteAdapter);
				//((Spinner) findViewById(R.id.layout_white_team).findViewById(R.id.spinner_team)).setOnItemSelectedListener(null);
				((Spinner) findViewById(R.id.slide_left_white).findViewById(R.id.spinner_team)).setAdapter(whiteAdapter);
				((Spinner) findViewById(R.id.slide_left_white).findViewById(R.id.spinner_team)).setOnItemSelectedListener(null);
				
				//((Spinner) findViewById(R.id.layout_red_team).findViewById(R.id.spinner_team)).setEnabled(false);
				//((Spinner) findViewById(R.id.layout_white_team).findViewById(R.id.spinner_team)).setEnabled(false);
				((Spinner) findViewById(R.id.slide_left_red).findViewById(R.id.spinner_team)).setEnabled(false);
				((Spinner) findViewById(R.id.slide_left_white).findViewById(R.id.spinner_team)).setEnabled(false);
				
				
				// binding player & player views
				PlayerView playerViews[][] = new PlayerView[2][];
				playerViews[0] = _redPlayerViews;
				playerViews[1] = _whitePlayerViews;
				for (int i = 0; i < 2; i++) {
					for (int j = 0; j < _ballNumber; j++) {
						PlayerView playerView = playerViews[i][j];
						WBTeam team = (i == 0) ? _match.getRedTeam() : _match.getWhiteTeam();
						WBPlayer player = team.getPlayerAtNumber(playerView.getPlayerNumber());
						playerView.setTag(player);
						playerView.setPlayerName(player.getName());
					}
				}

				// update player views
				_match.setCurrentTurnIndex(0);
				updateViewsAfterTurn();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void onCloseGameClicked(View view) {
		if (_match != null & _match.getWinnerTeam() == TeamColor.Undefined)
		{
			AlertDialog alert = DialogFactory.getQuestionDialog(this,
					getString(R.string.title_notice),
					getString(R.string.message_close_match), new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							setGameState(GameState.Closed);
							clearCount();
						}
					}, null);
			alert.show();
		}
		else
		{
			setGameState(GameState.Closed);
			clearCount();
		}
	}

	public void onSaveGameClicked(View view) {
		DialogFactory.getInputTextDialog(this, R.string.title_input_file, "",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						EditText editText = (EditText) ((AlertDialog) dialog)
								.findViewById(R.id.edit_name);
						String fileName = editText.getText().toString();

						String path = Utils.getDataFolder() + "/" + fileName;
						File file = new File(path);

						if (file.exists()) {
							DialogFactory.showWrongFileName(MainActivity.this, fileName, null);
						} else {
							try {
								_match.saveToFile(path);
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
					}
				}, null).show();
	}

	public void onLuckyTeamClicked(View view) {
		Random random = new Random();
		final TeamColor luckyTeam = random.nextBoolean() ? TeamColor.Red : TeamColor.White;
		DialogFactory.showLuckyTeamAlert(this, luckyTeam, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				ToggleButton toggleWhiteFirst = (ToggleButton) findViewById(R.id.toggle_white_first);
				toggleWhiteFirst.setChecked(luckyTeam == TeamColor.White);
			}
		});
	}

	/*
	 * Turn Controls
	 */
	public void onTimeMinusClicked(View view) {
		TextView tv = (TextView) findViewById(R.id.text_count);
		int curVal = Integer.valueOf(tv.getText().toString());
		curVal -= DEFAULT_TIME_STEP;

		tv.setText(String.valueOf(curVal));
		_settings.setCountDown(curVal);
		findViewById(R.id.button_time_minus).setEnabled(curVal > MIN_COUNT);
	}

	public void onTimePlusClicked(View view) {
		TextView tv = (TextView) findViewById(R.id.text_count);
		int curVal = Integer.valueOf(tv.getText().toString());
		curVal += DEFAULT_TIME_STEP;
		tv.setText(String.valueOf(curVal));
		_settings.setCountDown(curVal);
		findViewById(R.id.button_time_minus).setEnabled(curVal > MIN_COUNT);
	}

	public void onChangeTurnClicked(View view) {
		if (_gameState != GameState.Playing)
			return;

		playClickButtonSound();
		WBTurn turn = new WBTurn(null, TurnType.Empty);
		_match.addTurn(turn);

		clearCount();

		updateViewsAfterTurn();
		_hitChain.clear();
	}

	private void clearCount() {
		// clear count
		_countDownHandler.removeMessages(0);
		_count = 0;

		((TextView) findViewById(R.id.text_count))
				.setText(String.valueOf(_settings.getCountDown()));
	}

	public void onSkipTurnClicked(View view) {
		if (_gameState != GameState.Playing)
			return;

		if (_hitChain.size() < 1) {
			DialogFactory.showInvalidMoveAlert(this, null);
			return;
		}

		
		clearCount();

		WBTurn turn = new WBTurn(_hitChain.get(0), TurnType.Skip);
		_match.addTurn(turn);

	    if (turn.starter.getTeam().getSkipTurn() == 3) {
	        playWarningSound();
	    } else {
	    	playClickButtonSound();
	    }
		
		updateViewsAfterTurn();
		_hitChain.clear();
	}

	public void onHitTurnClicked(View view) {
		if (_gameState != GameState.Playing)
			return;

		if (_match.getWinnerTeam() != TeamColor.Undefined) {
			DialogFactory.showGameOverAlert(this, null);
			return;
		}

		if (_hitChain.size() < 2) {
			DialogFactory.showInvalidMoveAlert(this, null);
			return;
		}

		playClickButtonSound();
		clearCount();

		WBTurn turn = new WBTurn(_hitChain.get(0), TurnType.Hit);
		turn.hitChain.addAll(_hitChain.subList(1, _hitChain.size()));
		_match.addTurn(turn);

		updateViewsAfterTurn();
		_hitChain.clear();

		if (_match.getWinnerTeam() != TeamColor.Undefined) {
			DialogFactory.showWinnerAlert(this, _match.getWinnerTeam(), new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					onSaveGameClicked(null);
				}
				
			}, null);
		}
	}

	public void onIndivTurnClicked(View view) {
		if (_gameState != GameState.Playing)
			return;

		if (_hitChain.size() < 1) {
			DialogFactory.showInvalidMoveAlert(this, null);
			return;
		}
		
		playClickButtonSound();
		clearCount();

		WBTurn turn = new WBTurn(_hitChain.get(0), TurnType.Indiv);
		_match.addTurn(turn);

		updateViewsAfterTurn();
		_hitChain.clear();
	}

	public void onBackClicked(View view) {
		if (_match == null)
			return;

		_match.backTurn();

		if (_match.getCurrentTurnIndex() < _ballNumber * 2)
			_gameState = GameState.Setupping;

		updateViewsAfterTurn();
		clearCount();
	}

	public void onNextClicked(View view) {
		if (_match == null)
			return;

		_match.nextTurn();

		if (_match.getCurrentTurnIndex() >= _ballNumber * 2)
			_gameState = GameState.Playing;

		updateViewsAfterTurn();

		clearCount();
	}

	public void onCountClicked(View view) {
		if (_gameState != GameState.Playing)
			return;

		if (_count == 0) {
			// start count
			_count = _settings.getCountDown();
			_countDownHandler.sendEmptyMessageDelayed(0, 1000);
		} else
			clearCount();
	}

	public void onMissTurnClicked(View view) {
		if (_gameState != GameState.Playing)
			return;

		if (_hitChain.size() < 1) {
			DialogFactory.showInvalidMoveAlert(this, null);
			return;
		}

		playClickButtonSound();
		clearCount();

		WBTurn turn = new WBTurn(_hitChain.get(0), TurnType.Miss);
		_match.addTurn(turn);

		updateViewsAfterTurn();
		_hitChain.clear();
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
		switch (parent.getId()) {
		case R.id.spinner_ball_number:
			// change number of players
			// pos = 0 -> balls = 5
			// pos = 1 -> balls = 6
			// pos = 2 -> balls = 7
			_ballNumber = pos + 5;
			setupPlayerViewsForBallCount(_ballNumber);
			_settings.setBallCount(_ballNumber);
			break;
		case R.id.spinner_setup:
			if (_gameState != GameState.Setupping)
				return;

			PlayerView playerView = (PlayerView) parent.getTag();
			WBPlayer player = (WBPlayer) playerView.getTag();

			WBTurn turn = new WBTurn(player, TurnType.Setup);
			turn.setupState = playerView.getSetupState();

			if (turn.setupState == SetupState.Attack) 
				playAttackSound();
			else if (turn.setupState == SetupState.Defence)
				playDefenseSound();
			
			_match.addTurn(turn);

			// TODO: not hard code
			if (player.getTeam().getNumberOfActacks() > _ballNumber - 2) {
				DialogFactory.showInvalidSetupAlert(this, SetupState.Attack, null);
				playerView.setSetupState(SetupState.Undefined);
				_match.backTurn();
			} else if (player.getTeam().getNumberOfDefences() > _ballNumber - 2) {
				DialogFactory.showInvalidSetupAlert(this, SetupState.Defence, null);
				playerView.setSetupState(SetupState.Undefined);
				_match.backTurn();
			} else {
				if (_match.isSetupComplete()) {
					setGameState(GameState.Playing);
				}

				updateViewsAfterTurn();
			}

			break;
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {

	}

	/**
	 * Allow match setting controls enabled or not.
	 * 
	 * @param enabled
	 */
	private void setMatchSettingEnable(boolean enabled) {
		findViewById(R.id.spinner_ball_number).setEnabled(enabled);
		findViewById(R.id.toggle_white_first).setEnabled(enabled);
		
//		findViewById(R.id.layout_red_team).findViewById(R.id.spinner_team).setEnabled(enabled);
//		findViewById(R.id.layout_white_team).findViewById(R.id.spinner_team).setEnabled(enabled);
		
		findViewById(R.id.slide_left_red).findViewById(R.id.spinner_team).setEnabled(enabled);
		findViewById(R.id.slide_left_white).findViewById(R.id.spinner_team).setEnabled(enabled);
	}

	private void setTurnControlsEnable(boolean enabled) {
		findViewById(R.id.button_change_turn).setEnabled(enabled);
		findViewById(R.id.button_hit_turn).setEnabled(enabled);
		findViewById(R.id.button_miss_turn).setEnabled(enabled);
		findViewById(R.id.button_skip_turn).setEnabled(enabled);
		findViewById(R.id.button_indiv_turn).setEnabled(enabled);
		findViewById(R.id.button_count).setEnabled(enabled);

		findViewById(R.id.button_back).setEnabled(enabled);
		findViewById(R.id.button_next).setEnabled(enabled);

		findViewById(R.id.button_red_captute_zone_minus).setEnabled(enabled);
		findViewById(R.id.button_red_captute_zone_plus).setEnabled(enabled);
		findViewById(R.id.button_red_fault_turn_minus).setEnabled(enabled);
		findViewById(R.id.button_red_fault_turn_plus).setEnabled(enabled);

		findViewById(R.id.button_white_captute_zone_minus).setEnabled(enabled);
		findViewById(R.id.button_white_captute_zone_plus).setEnabled(enabled);
		findViewById(R.id.button_white_fault_turn_minus).setEnabled(enabled);
		findViewById(R.id.button_white_fault_turn_plus).setEnabled(enabled);
	}

	private void setGameState(GameState gameState) {
		_gameState = gameState;

		switch (gameState) {
		case Setupping:
			findViewById(R.id.button_new_game).setEnabled(false);
			findViewById(R.id.button_open_game).setEnabled(false);
			findViewById(R.id.button_close_game).setEnabled(true);
			findViewById(R.id.button_lucky_team).setEnabled(false);
			findViewById(R.id.button_save_game).setEnabled(true);
			setMatchSettingEnable(false);
			setTurnControlsEnable(false);
			break;
		case Opening:
			findViewById(R.id.button_new_game).setEnabled(false);
			findViewById(R.id.button_open_game).setEnabled(false);
			findViewById(R.id.button_close_game).setEnabled(true);
			findViewById(R.id.button_lucky_team).setEnabled(false);
			findViewById(R.id.button_save_game).setEnabled(false);
			setMatchSettingEnable(false);
			setTurnControlsEnable(false);
			break;
		case Playing:
			findViewById(R.id.button_new_game).setEnabled(false);
			findViewById(R.id.button_open_game).setEnabled(false);
			findViewById(R.id.button_close_game).setEnabled(true);
			findViewById(R.id.button_lucky_team).setEnabled(false);
			findViewById(R.id.button_save_game).setEnabled(true);
			setMatchSettingEnable(false);
			setTurnControlsEnable(true);
			break;
		case Closed:
			findViewById(R.id.button_new_game).setEnabled(true);
			findViewById(R.id.button_open_game).setEnabled(true);
			findViewById(R.id.button_close_game).setEnabled(false);
			findViewById(R.id.button_lucky_team).setEnabled(true);
			findViewById(R.id.button_save_game).setEnabled(false);
			setMatchSettingEnable(true);
			setTurnControlsEnable(false);

			resetViewsBeforeMatch();
			break;
		default:
			break;
		}
	}

	private void loadTeamListForSpinner(Spinner teamSpinner, OnItemSelectedListener listener) {
		final List<TeamContract> teams = _dbHelper.getAllTeams();
		TeamContract noTeam = new TeamContract();
		noTeam.id = -1;
		
		Resources res = Utils.getResourcesForLocale(getResources(), Utils.getLocale(_settings.getLanguage()));
		
		noTeam.name = res.getString(R.string.label_select_team);
		teams.add(0, noTeam);
		ArrayAdapter<TeamContract> adapter = new ArrayAdapter<TeamContract>(this,
				android.R.layout.simple_spinner_item, android.R.id.text1, teams) {
			@Override
			public long getItemId(int position) {
				return teams.get(position).id;
			};
		};
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		teamSpinner.setAdapter(adapter);
		teamSpinner.setOnItemSelectedListener(listener);
		
		
	}

	/*
	 * Bottom View - Button Clicked Processing
	 */

	private void increaseNumber(int textId, int relatedMinusButtonId) {
		TextView tv = (TextView) findViewById(textId);
		int curVal = Integer.valueOf(tv.getText().toString());
		curVal++;
		tv.setText(String.valueOf(curVal));

		findViewById(relatedMinusButtonId).setEnabled(curVal > 0);
	}

	private void decreaseNumber(int textId, int relatedMinusButtonId) {
		TextView tv = (TextView) findViewById(textId);
		int curVal = Integer.valueOf(tv.getText().toString());
		curVal--;

		tv.setText(String.valueOf(curVal));
		findViewById(relatedMinusButtonId).setEnabled(curVal > 0);
	}

	public void onRedFaultTurnMinusClicked(View view) {
		decreaseNumber(R.id.text_red_fault_turn, R.id.button_red_fault_turn_minus);
		
		if (_match != null) {
			TextView tv = (TextView) findViewById(R.id.text_red_fault_turn);
			_match.getRedTeam().setTeamFault(Integer.valueOf(tv.getText().toString()));
		}
	}

	public void onRedFaultTurnPlusClicked(View view) {
		increaseNumber(R.id.text_red_fault_turn, R.id.button_red_fault_turn_minus);
		
		if (_match != null) {
			TextView tv = (TextView) findViewById(R.id.text_red_fault_turn);
			_match.getRedTeam().setTeamFault(Integer.valueOf(tv.getText().toString()));
		}
	}

	public void onWhiteFaultTurnMinusClicked(View view) {
		decreaseNumber(R.id.text_white_fault_turn, R.id.button_white_fault_turn_minus);
		
		if (_match != null) {
			TextView tv = (TextView) findViewById(R.id.text_white_fault_turn);
			_match.getWhiteTeam().setTeamFault(Integer.valueOf(tv.getText().toString()));
		}
	}

	public void onWhiteFaultTurnPlusClicked(View view) {
		increaseNumber(R.id.text_white_fault_turn, R.id.button_white_fault_turn_minus);
		
		if (_match != null) {
			TextView tv = (TextView) findViewById(R.id.text_white_fault_turn);
			_match.getWhiteTeam().setTeamFault(Integer.valueOf(tv.getText().toString()));
		}
	}

	public void onRedCaptuteZoneMinusClicked(View view) {
		decreaseNumber(R.id.text_red_captute_zone, R.id.button_red_captute_zone_minus);
	}

	public void onRedCaptuteZonePlusClicked(View view) {
		increaseNumber(R.id.text_red_captute_zone, R.id.button_red_captute_zone_minus);
	}

	public void onWhiteCaptuteZoneMinusClicked(View view) {
		decreaseNumber(R.id.text_white_captute_zone, R.id.button_white_captute_zone_minus);
	}

	public void onWhiteCaptuteZonePlusClicked(View view) {
		increaseNumber(R.id.text_white_captute_zone, R.id.button_white_captute_zone_minus);
	}
	
	public void onLanguageClicked(View view) {
		int language = _settings.getLanguage();
		switch (language) {
		case SettingController.LANGUAGE_ENGLISH:
			language = SettingController.LANGUAGE_VIETNAMESE;
			break;
		case SettingController.LANGUAGE_VIETNAMESE:
			language = SettingController.LANGUAGE_CHINESE;
			break;
		case SettingController.LANGUAGE_CHINESE:
			language = SettingController.LANGUAGE_ENGLISH;
			break;
		default:
			break;
		}
		_settings.setLanguage(language);
		updateLanguage(Utils.getLocale(language));
	}
	
	public void onAboutClicked(View view) {
		DialogFactory.showInfoDialog(this, _versionCode);
	}
	
	public void updateLanguage(Locale locale) {
		Resources res = Utils.getResourcesForLocale(getResources(), locale);
		
		ToggleButton toggleButton = (ToggleButton) findViewById(R.id.toggle_white_first);
		toggleButton.setTextOn(getString(R.string.label_white));
		toggleButton.setTextOff(getString(R.string.label_red));
		
		if (locale == Locale.US)
			((Button) findViewById(R.id.button_language)).setText(R.string.label_en);
		else if (locale == Locale.CHINESE)
			((Button) findViewById(R.id.button_language)).setText(R.string.label_zh);
		else
			((Button) findViewById(R.id.button_language)).setText(R.string.label_vn);
		
		((TextView) findViewById(R.id.label_time)).setText(res.getString(R.string.label_time));
		((TextView) findViewById(R.id.label_match_time)).setText(res.getString(R.string.label_match_time));
		((TextView) findViewById(R.id.label_number_of_players)).setText(res.getString(R.string.label_balls));
		((TextView) findViewById(R.id.label_white_first)).setText(res.getString(R.string.label_white_first));

		((Button) findViewById(R.id.button_new_game)).setText(res.getString(R.string.label_new_game));
		((Button) findViewById(R.id.button_save_game)).setText(res.getString(R.string.label_save_game));
		((Button) findViewById(R.id.button_open_game)).setText(res.getString(R.string.label_open_game));
		((Button) findViewById(R.id.button_close_game)).setText(res.getString(R.string.label_close_game));
		((Button) findViewById(R.id.button_lucky_team)).setText(res.getString(R.string.label_lucky_team));
		
		((TextView) findViewById(R.id.label_red_ball)).setText(res.getString(R.string.label_ball));
		((TextView) findViewById(R.id.label_white_ball)).setText(res.getString(R.string.label_ball));
		((TextView) findViewById(R.id.label_red_ball_rescued)).setText(res.getString(R.string.label_ball_rescued, getString(R.string.label_red_name)));
		((TextView) findViewById(R.id.label_white_ball_rescued)).setText(res.getString(R.string.label_ball_rescued, getString(R.string.label_white_name)));
		
		((Button) findViewById(R.id.button_indiv_turn)).setText(res.getString(R.string.label_indiv_turn));
		((Button) findViewById(R.id.button_hit_turn)).setText(res.getString(R.string.label_hit_turn));
		((Button) findViewById(R.id.button_miss_turn)).setText(res.getString(R.string.label_miss_turn));
		((Button) findViewById(R.id.button_skip_turn)).setText(res.getString(R.string.label_skip_turn));
		((Button) findViewById(R.id.button_change_turn)).setText(res.getString(R.string.label_change_turn));
		
		((TextView) findViewById(R.id.label_current_turn)).setText(res.getString(R.string.label_current_turn));
		((Button) findViewById(R.id.button_count)).setText(res.getString(R.string.label_count));
		((Button) findViewById(R.id.button_next)).setText(res.getString(R.string.label_next));
		((Button) findViewById(R.id.button_back)).setText(res.getString(R.string.label_back));
		
		((TextView) findViewById(R.id.label_red_fault_turn)).setText(res.getString(R.string.label_red_fault_turn));
		((TextView) findViewById(R.id.label_white_fault_turn)).setText(res.getString(R.string.label_white_fault_turn));
		((TextView) findViewById(R.id.label_red_captute_zone)).setText(res.getString(R.string.label_red_captute_zone));
		((TextView) findViewById(R.id.label_white_captute_zone)).setText(res.getString(R.string.label_white_captute_zone));
		
		View redTeamLayout = findViewById(R.id.slide_left_red);
		loadTeamListForSpinner((Spinner) redTeamLayout.findViewById(R.id.spinner_team),
				_onRedTeamSelectedListener);
		((TextView) redTeamLayout.findViewById(R.id.label_skip_turn_brief)).setText(res.getString(R.string.label_skip_turn_brief));
		((TextView) redTeamLayout.findViewById(R.id.label_skip_turn_fault_brief)).setText(res.getString(R.string.label_skip_turn_fault_brief));
		
		View whiteTeamLayout = findViewById(R.id.slide_left_white);
		loadTeamListForSpinner((Spinner) whiteTeamLayout.findViewById(R.id.spinner_team),
				_onWhiteTeamSelectedListener);
		((TextView) whiteTeamLayout.findViewById(R.id.label_skip_turn_brief)).setText(res.getString(R.string.label_skip_turn_brief));
		((TextView) whiteTeamLayout.findViewById(R.id.label_skip_turn_fault_brief)).setText(res.getString(R.string.label_skip_turn_fault_brief));
	}
	
	void playDefenseSound() {
		MediaPlayer mp = MediaPlayer.create(this, R.raw.defense);   
        mp.start();
	}
	
	void playAttackSound() {
		MediaPlayer mp = MediaPlayer.create(this, R.raw.attack);   
        mp.start();
	}
	
	void playWarningSound() {
		MediaPlayer mp = MediaPlayer.create(this, R.raw.warning);   
        mp.start();
	}
	
	void playClickButtonSound() {
		MediaPlayer mp = MediaPlayer.create(this, R.raw.button);   
        mp.start();
	}
	
	void playClickBallSound() {
		MediaPlayer mp = MediaPlayer.create(this, R.raw.ball);   
        mp.start();
	}
	
	public void onMoreClicked(View view) {
		setLess(!_isLess);
	}
	
	void setLess(boolean isLess) {
		_isLess = isLess;
		
		findViewById(R.id.label_red_fault_turn).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
		findViewById(R.id.label_white_fault_turn).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.text_red_fault_turn).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.text_white_fault_turn).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.button_red_fault_turn_plus).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.button_red_fault_turn_minus).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.button_white_fault_turn_plus).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.button_white_fault_turn_minus).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.label_red_captute_zone).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.label_white_captute_zone).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.text_red_captute_zone).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.text_white_captute_zone).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.button_red_captute_zone_minus).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.button_red_captute_zone_plus).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.button_white_captute_zone_minus).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.button_white_captute_zone_plus).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    
	    findViewById(R.id.button_language).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.button_about).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.label_time).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.label_match_time).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.text_time).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.text_match_time).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    
	    for (int i = 0; i < _ballNumber; i++) {
	        	_redPlayerViews[i].setNameVisible(!isLess);
	        	_whitePlayerViews[i].setNameVisible(!isLess);
	    }
	    
//	    findViewById(R.id.layout_red_team).findViewById(R.id.spinner_team).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
//	    findViewById(R.id.layout_white_team).findViewById(R.id.spinner_team).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    
	    findViewById(R.id.slide_left_red).findViewById(R.id.spinner_team).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    findViewById(R.id.slide_left_white).findViewById(R.id.spinner_team).setVisibility(isLess ? View.INVISIBLE : View.VISIBLE);
	    
//	    Button buttonMore = (Button) findViewById(R.id.button_more);
//	    if (isLess)
//	    	buttonMore.setText(R.string.label_more);
//	    else
//	    	buttonMore.setText(R.string.label_less);
	    mDrawer.setOnDrawerOpenListener(new OnDrawerOpenListener() {

            @Override
            public void onDrawerOpened() {
                Log.d("Debug","Drawer Opened");
                mDrawer_1.setVisibility(View.GONE);
                mDrawer_2.setVisibility(View.GONE);
        }
        });

        mDrawer.setOnDrawerCloseListener(new OnDrawerCloseListener() {

            @Override
            public void onDrawerClosed() {
                Log.d("Debug","Drawer Closed");
                mDrawer_1.setVisibility(View.VISIBLE);
                mDrawer_2.setVisibility(View.VISIBLE);
            }
        });
	    
        // Set visible slider top
        mDrawer_1.setOnDrawerOpenListener(new OnDrawerOpenListenerBottom() {
			
			@Override
			public void onDrawerOpened() {
				// TODO Auto-generated method stub
				mDrawer.setVisibility(View.GONE);
				 mDrawer_2.setVisibility(View.GONE);
			}
		});
        
        mDrawer_1.setOnDrawerCloseListener(new OnDrawerCloseListenerBottom() {
			
			@Override
			public void onDrawerClosed() {
				// TODO Auto-generated method stub
				mDrawer.setVisibility(View.VISIBLE);
				 mDrawer_2.setVisibility(View.VISIBLE);
			}
		});
        
        mDrawer_2.setOnDrawerOpenListener(new OnDrawerOpenListenerLeft() {
			
			@Override
			public void onDrawerOpened() {
				// TODO Auto-generated method stub
				mDrawer.setVisibility(View.GONE);
				 mDrawer_1.setVisibility(View.GONE);
			}
		});
        
        mDrawer_2.setOnDrawerCloseListener(new OnDrawerCloseListenerLeft() {
			
			@Override
			public void onDrawerClosed() {
				// TODO Auto-generated method stub
				mDrawer.setVisibility(View.VISIBLE);
				 mDrawer_1.setVisibility(View.VISIBLE);
			}
		});
			
		
       
	}

	@Override
	public void onContentChanged() {
		// TODO Auto-generated method stub
		super.onContentChanged();
		mDrawer = (MultiDirectionSlidingDrawerTop)findViewById(R.id.drawer);
		mDrawer_1 = (MultiDirectionSlidingDrawerBottom)findViewById(R.id.drawer_1);
		mDrawer_2 = (MultiDirectionSlidingDrawerLeft)findViewById(R.id.drawer_2);
	}
	
	
	//slider
	
	
	
}
