package com.goodapps.wiserball.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;

import com.goodapps.wiserball.R;
import com.goodapps.wiserball.type.TeamColor;

public class WhitePlayerView extends PlayerView{
	public WhitePlayerView(Context context) {
		this(context, null);
	}

	public WhitePlayerView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public WhitePlayerView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		LayoutInflater.from(context).inflate(R.layout.white_view_player, this);
		
		setupControls();
		setTeamColor(TeamColor.White);
	}
}
