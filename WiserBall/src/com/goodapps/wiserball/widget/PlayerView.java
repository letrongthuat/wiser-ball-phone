package com.goodapps.wiserball.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.goodapps.wiserball.R;
import com.goodapps.wiserball.type.PlayerRole;
import com.goodapps.wiserball.type.PlayerState;
import com.goodapps.wiserball.type.SetupState;
import com.goodapps.wiserball.type.TeamColor;

public class PlayerView extends RelativeLayout {

	private int _playerNumber;
	private TeamColor _teamColor;

	private Drawable[] _setupImages = { getResources().getDrawable(R.drawable.setup_default),
			getResources().getDrawable(R.drawable.setup_attack),
			getResources().getDrawable(R.drawable.setup_defence) };

	public PlayerView(Context context) {
		this(context, null);
	}

	public PlayerView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public PlayerView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	protected void setupControls() {
		Spinner spinner = (Spinner) findViewById(R.id.spinner_setup);
		ArrayAdapter<Drawable> adapter = new ArrayAdapter<Drawable>(getContext(), 0, _setupImages) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				return getCustomView(position, convertView, parent);
			}

			@Override
			public View getDropDownView(int position, View convertView, ViewGroup parent) {
				return getCustomView(position, convertView, parent);
			}

			@SuppressWarnings("deprecation")
			public View getCustomView(int position, View convertView, ViewGroup parent) {
				if (convertView == null) {
					LayoutInflater inflater = LayoutInflater.from(getContext());
					convertView = inflater.inflate(android.R.layout.simple_spinner_item, parent,
							false);
				}

				TextView label = (TextView) convertView.findViewById(android.R.id.text1);
				label.setBackgroundDrawable(getItem(position));
				label.getLayoutParams().width = MeasureSpec.makeMeasureSpec((int) getResources()
						.getDimension(R.dimen.image_setup_width), MeasureSpec.EXACTLY);
				label.getLayoutParams().height = MeasureSpec.makeMeasureSpec((int) getResources()
						.getDimension(R.dimen.image_setup_height), MeasureSpec.EXACTLY);
				return convertView;
			}
		};
		spinner.setAdapter(adapter);
		
		// tag the parent here
		spinner.setTag(this);
		findViewById(R.id.button_player).setTag(this);
	}
	
	public void setPlayerNumber(int playerNo) {
		_playerNumber = playerNo;

		Button button = (Button) findViewById(R.id.button_player);
		button.setText(String.valueOf(playerNo));
	}

	public int getPlayerNumber() {
		return _playerNumber;
	}

	public void setOnPlayerClickListener(OnClickListener listener) {
		findViewById(R.id.button_player).setOnClickListener(listener);
	}

	public void setOnSetupSelectListener(OnItemSelectedListener listener) {
		((Spinner) findViewById(R.id.spinner_setup)).setOnItemSelectedListener(listener);
	}

	public Button getPlayerButton() {
		return (Button) findViewById(R.id.button_player);
	}

	public void setIndiv(int indiv) {
		TextView text = (TextView) findViewById(R.id.textIndiv);
		text.setText(String.valueOf(indiv));
	}

	public void setControlEnabled(boolean enabled) {
		findViewById(R.id.button_player).setEnabled(enabled);
	}

	public boolean isControlEnabled() {
		return findViewById(R.id.button_player).isEnabled();
	}

	public void setSetupEnabled(boolean enabled) {
		View setup = findViewById(R.id.spinner_setup);
		setup.setEnabled(enabled);
	}

	public void setSetupVisible(boolean visible) {
		View setup = findViewById(R.id.spinner_setup);
		setup.setVisibility(visible ? VISIBLE : GONE);
	}

	public void setSetupState(SetupState setupState) {
		final Spinner view = (Spinner) findViewById(R.id.spinner_setup);
		final OnItemSelectedListener savedListener = view.getOnItemSelectedListener();
		view.setOnItemSelectedListener(null);
		switch (setupState) {
		case Undefined:
			view.setSelection(0);
			break;
		case Attack:
			view.setSelection(1);
			break;
		case Defence:
			view.setSelection(2);
			break;
		}
		view.post(new Runnable() {
			@Override
			public void run() {
				view.setOnItemSelectedListener(savedListener);
			}
		});
	}

	public SetupState getSetupState() {
		Spinner view = (Spinner) findViewById(R.id.spinner_setup);

		switch (view.getSelectedItemPosition()) {
		case 0:
			return SetupState.Undefined;
		case 1:
			return SetupState.Attack;
		case 2:
			return SetupState.Defence;
		default:
			return SetupState.Undefined;
		}
	}

	public boolean isSetupEnabled() {
		View setup = findViewById(R.id.spinner_setup);
		return setup.isEnabled();
	}

	public void setTeamColor(TeamColor color) {
		_teamColor = color;
		
		Button buttonPlayer = (Button) findViewById(R.id.button_player); 
		
//		ImageView imageTeam = (ImageView) findViewById(R.id.image_team);
		if (color == TeamColor.Red) {
			buttonPlayer.setBackgroundResource(R.drawable.red_button);
			buttonPlayer.setTextColor(0xFFFFFFFF);
//			imageTeam.setImageResource(R.drawable.team_red);
		} else if (color == TeamColor.White) {
			buttonPlayer.setBackgroundResource(R.drawable.white_button);
			buttonPlayer.setTextColor(0xFFFF0000);
//			imageTeam.setImageResource(R.drawable.team_white);
		}
	}

	public TeamColor getTeamColor() {
		return _teamColor;
	}

	public void setHit1Hidden(boolean hidden) {
		findViewById(R.id.textHit1).setVisibility(hidden ? View.INVISIBLE : View.VISIBLE);
		findViewById(R.id.textHitIndex1).setVisibility(hidden ? View.INVISIBLE : View.VISIBLE);
	}

	public void setHit2Hidden(boolean hidden) {
		findViewById(R.id.textHit2).setVisibility(hidden ? View.INVISIBLE : View.VISIBLE);
		findViewById(R.id.textHitIndex2).setVisibility(hidden ? View.INVISIBLE : View.VISIBLE);
	}

	public void setTeamHit1Hidden(boolean hidden) {
		findViewById(R.id.textTeam1).setVisibility(hidden ? View.INVISIBLE : View.VISIBLE);
	}

	public void setTeamHit2Hidden(boolean hidden) {
		findViewById(R.id.textTeam2).setVisibility(hidden ? View.INVISIBLE : View.VISIBLE);
	}

	public void setFirstHitReceivedFromTeamString(String value) {
		TextView text = (TextView) findViewById(R.id.textTeam1);
		text.setText(value);
	}

	public void setSecondHitReceivedFromTeamString(String value) {
		TextView text = (TextView) findViewById(R.id.textTeam2);
		text.setText(value);
	}

	public void setPlayerNumberOfFirstHitFromOpponent(int playerNumber) {
		TextView text = (TextView) findViewById(R.id.textHit1);
		text.setText(String.valueOf(playerNumber));
	}

	public void setFirstHitReceivedFromOpponentIndex(int index) {
		TextView text = (TextView) findViewById(R.id.textHitIndex1);
		text.setText(String.valueOf(index));

		setHit1Hidden(index == 0);
	}

	public void setPlayerNumberOfSecondHitFromOpponent(int playerNumber) {
		TextView text = (TextView) findViewById(R.id.textHit2);
		text.setText(String.valueOf(playerNumber));
	}

	public void setSecondHitReceivedFromOpponentIndex(int index) {
		TextView text = (TextView) findViewById(R.id.textHitIndex2);
		text.setText(String.valueOf(index));

		setHit2Hidden(index == 0);
	}

	public void setPlayerState(PlayerState playerState) {
		ImageView imageState = (ImageView) findViewById(R.id.image_team);
		Button button = (Button) findViewById(R.id.button_player);
		switch (playerState) {
		case Green:
			imageState.setImageResource(R.drawable.state_green);
			setTeamColor(_teamColor);
			break;
		case Yellow:
			imageState.setImageResource(R.drawable.state_yellow);
			setTeamColor(_teamColor);
			break;
		case Red:
			imageState.setImageResource(R.drawable.state_red);
			setTeamColor(_teamColor);
			break;
		default:
			button.setBackgroundResource(R.drawable.state_out);
			if (_teamColor == TeamColor.Red)
				imageState.setImageResource(R.drawable.team_red);
			else
				imageState.setImageResource(R.drawable.team_white);
			break;
		}
	}

	public void setRole(PlayerRole role) {
		findViewById(R.id.image_role).setVisibility(role == PlayerRole.Captain ? VISIBLE : GONE);
	}

	public void setPlayerName(CharSequence name) {
		TextView tv = (TextView) findViewById(R.id.text_player_name);
		tv.setText(name);
	}
	
	public CharSequence getPlayerName(String name) {
		TextView tv = (TextView) findViewById(R.id.text_player_name);
		return tv.getText();
	}
	
	public void setNameVisible(boolean isVisible) {
		findViewById(R.id.text_player_name).setVisibility(isVisible ? View.VISIBLE : View.INVISIBLE);
	}
}
