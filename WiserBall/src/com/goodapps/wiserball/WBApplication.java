package com.goodapps.wiserball;

import android.app.Application;

public class WBApplication extends Application {
	private static WBApplication _sharedInstance;
	
	@Override
	public void onCreate() {
		super.onCreate();
		
		_sharedInstance = this;
	}
	public static WBApplication getSharedApplication()
	{
		return _sharedInstance;
	}
}
