package com.goodapps.wiserball;

import java.io.IOException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

import com.goodapps.wiserball.game.WBMatch;

public class FileActivity extends Activity implements OnItemClickListener {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_file);
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
		boolean isSelect = getIntent().getBooleanExtra(MainActivity.EXTRA_SELECT_FILE, false);

		TextView text = (TextView) view.findViewById(android.R.id.text1);
		String path = Utils.getDataFolder() + "/" + text.getText();

		if (isSelect) {
			Intent data = new Intent();
			data.putExtra(MainActivity.EXTRA_SELECT_FILE, path);
			setResult(RESULT_OK, data);
			finish();
		} else {
			readFile(path);
		}
	}

	private void readFile(String path) {
		try {
			WBMatch match = WBMatch.openFromFile(path);

			StatiticsFragment fragment = (StatiticsFragment) getFragmentManager().findFragmentById(
					R.id.fragment_file_details);

			fragment.setRedTeamName(match.getRedTeam().getName());
			fragment.setWhiteTeamName(match.getWhiteTeam().getName());
			fragment.setRedTurns(match.getTurnCountOfTeam(match.getRedTeam()));
			fragment.setWhiteTurns(match.getTurnCountOfTeam(match.getWhiteTeam()));
			fragment.setRedTeamFault(match.getRedTeam().getTeamFault());
			fragment.setWhiteTeamFault(match.getWhiteTeam().getTeamFault());
			fragment.setTeamWin(match.getWinnerTeam());
			
			fragment.removeAllRows(R.id.red_statitics);
			fragment.removeAllRows(R.id.white_statitics);

			for (int i = 0; i < match.getNumberOfPlayers(); i++) {
				fragment.addRow(R.id.red_statitics, match.getRedTeam().getPlayerAtNumber(i + 1)
						.getName(), match.getTotalTurnsOfPlayer(i + 1, match.getRedTeam()),
						match.getHitTurnsOfPlayer(i + 1, match.getRedTeam()),
						match.getMissTurnsOfPlayer(i + 1, match.getRedTeam()),
						match.getSkipTurnsOfPlayer(i + 1, match.getRedTeam()),
						match.getFaultTurnsOfPlayer(i + 1, match.getRedTeam()));

				fragment.addRow(R.id.white_statitics, match.getWhiteTeam().getPlayerAtNumber(i + 1)
						.getName(), match.getTotalTurnsOfPlayer(i + 1, match.getWhiteTeam()),
						match.getHitTurnsOfPlayer(i + 1, match.getWhiteTeam()),
						match.getMissTurnsOfPlayer(i + 1, match.getWhiteTeam()),
						match.getSkipTurnsOfPlayer(i + 1, match.getWhiteTeam()),
						match.getFaultTurnsOfPlayer(i + 1, match.getWhiteTeam()));
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
