package com.goodapps.wiserball;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.database.Cursor;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.goodapps.wiserball.data.DbHelper;
import com.goodapps.wiserball.data.PlayerContract;
import com.goodapps.wiserball.data.PlayerContract.PlayerEntry;
import com.goodapps.wiserball.data.TeamContract;
import com.mobeta.android.dslv.DragSortListView;
import com.mobeta.android.dslv.DragSortListView.DropListener;

public class TeamActivity extends Activity {

	private OnItemLongClickListener _onPlayerItemLongClickListener = new OnItemLongClickListener() {
		@Override
		public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
			if (_actionMode != null) {
				return false;
			}
			_actionMode = TeamActivity.this.startActionMode(new MyCallback(R.menu.menu_player, id));
			view.setSelected(true);

			return true;
		}
	};

	private OnItemLongClickListener _onTeamItemLongClickListener = new OnItemLongClickListener() {
		@Override
		public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
			if (_actionMode != null) {
				return false;
			}
			_actionMode = TeamActivity.this.startActionMode(new MyCallback(R.menu.menu_team, id));
			view.setSelected(true);

			return true;
		}
	};

	private OnItemClickListener _onTeamItemClickListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
			viewTeam(id);
		}
	};

	class MyCallback implements ActionMode.Callback {
		// private ActionMode.Callback _teamActionModeCallback = new
		// ActionMode.Callback() {
		private long _itemId;
		private int _menuResourceId;

		public MyCallback(int menuResourceId, long itemId) {
			_itemId = itemId;
			_menuResourceId = menuResourceId;
		}

		// Called when the action mode is created; startActionMode() was called
		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			// Inflate a menu resource providing context menu items
			MenuInflater inflater = mode.getMenuInflater();
			inflater.inflate(_menuResourceId, menu);
			return true;
		}

		// Called each time the action mode is shown. Always called after
		// onCreateActionMode, but
		// may be called multiple times if the mode is invalidated.
		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false; // Return false if nothing is done
		}

		// Called when the user selects a contextual menu item
		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			if (_menuResourceId == R.menu.menu_team) {
				switch (item.getItemId()) {
				case R.id.item_rename:
					renameTeam(_itemId);
					mode.finish(); // Action picked, so close the CAB
					return true;
				case R.id.item_delete:
					askForDeletingTeam(_itemId);
					mode.finish();
					return true;
				default:
					return false;
				}
			} else if (_menuResourceId == R.menu.menu_player) {
				switch (item.getItemId()) {
				case R.id.item_rename:
					renamePlayer(_itemId);
					mode.finish(); // Action picked, so close the CAB
					return true;
				case R.id.item_delete:
					askForDeletingPLayer(_itemId);
					mode.finish();
					return true;
				case R.id.item_captain:
					setCaptain(_itemId);
					mode.finish();
					return true;
				default:
					return false;
				}
			}
			return false;
		}

		// Called when the user exits the action mode
		@Override
		public void onDestroyActionMode(ActionMode mode) {
			_actionMode = null;
		}
	};

	private ActionMode _actionMode;
	private DbHelper _dbHelper;
	private CursorAdapter _teamAdapter;
	private CursorAdapter _playerAdapter;
	private long _currentTeam = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_team);

		_dbHelper = new DbHelper(this);

		ListView teamList = (ListView) findViewById(R.id.list_team);
		Cursor teamCursor = _dbHelper.getAllTeamsCursor();

		_teamAdapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_1,
				teamCursor, new String[] { TeamContract.TeamEntry.COLUMN_TEAM_NAME },
				new int[] { android.R.id.text1 },
				SimpleCursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
		teamList.setAdapter(_teamAdapter);
		teamList.setOnItemLongClickListener(_onTeamItemLongClickListener);
		teamList.setOnItemClickListener(_onTeamItemClickListener);

		DragSortListView playerList = (DragSortListView) findViewById(R.id.list_player);
		Cursor playerCursor = _dbHelper.getAllPlayersForTeamCursor(-1);

		_playerAdapter = new SimpleCursorAdapter(this, R.layout.list_item_handle_right,
				playerCursor, new String[] { PlayerEntry.COLUMN_PLAYER_NAME },
				new int[] { android.R.id.text1 },
				SimpleCursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				View v = super.getView(position, convertView, parent);

				long id = getItemId(position);
				PlayerContract player = _dbHelper.getPlayerById(id);
				v.findViewById(R.id.image_role).setVisibility(
						player.role == 1 ? View.VISIBLE : View.INVISIBLE);

				return v;

			}
		};
		playerList.setAdapter(_playerAdapter);
		playerList.setOnItemLongClickListener(_onPlayerItemLongClickListener);

		playerList.setDropListener(new DropListener() {
			@Override
			public void drop(int from, int to) {
				resortPlayer(from, to);
				_playerAdapter.changeCursor(_dbHelper.getAllPlayersForTeamCursor(_currentTeam));
				_playerAdapter.notifyDataSetChanged();
			}
		});
	}

	private void resortPlayer(int from, int to) {
		long playerId;
		PlayerContract player;
		if (from < to) {
			int run = from + 1;
			for (; run <= to; run++) {
				playerId = _playerAdapter.getItemId(run);
				player = _dbHelper.getPlayerById(playerId);
				player.order--;
				_dbHelper.updatePlayer(player);
			}
			playerId = _playerAdapter.getItemId(from);
			player = _dbHelper.getPlayerById(playerId);
			player.order = to;
			_dbHelper.updatePlayer(player);
		} else if (from > to) {
			int run = from - 1;
			for (; run >= to; run--) {
				playerId = _playerAdapter.getItemId(run);
				player = _dbHelper.getPlayerById(playerId);
				player.order++;
				_dbHelper.updatePlayer(player);
			}
			playerId = _playerAdapter.getItemId(from);
			player = _dbHelper.getPlayerById(playerId);
			player.order = to;
			_dbHelper.updatePlayer(player);
		}
	}

	private void viewTeam(long id) {
		_currentTeam = id;
		_playerAdapter.changeCursor(_dbHelper.getAllPlayersForTeamCursor(id));
		_playerAdapter.notifyDataSetChanged();

		TeamContract team = _dbHelper.getTeamById(id);
		TextView tv = (TextView) findViewById(R.id.text_title_player);
		if (team == null)
			tv.setText(R.string.label_players);
		else
			tv.setText(getString(R.string.label_players) + " - " + team.name);
	}

	public void onAddTeamClicked(View view) {
		AlertDialog inputDialog = DialogFactory.getInputTextDialog(this, R.string.title_input_team,
				"", new OnClickListener() {
					@Override
					public void onClick(DialogInterface parent, int which) {
						Dialog dialog = (Dialog) parent;
						TeamContract team = new TeamContract();
						EditText editText = (EditText) dialog.findViewById(R.id.edit_name);
						team.name = editText.getText().toString();
						_dbHelper.insertTeam(team);
						_teamAdapter.changeCursor(_dbHelper.getAllTeamsCursor());
						_teamAdapter.notifyDataSetChanged();
					}
				}, null);
		inputDialog.show();
	}

	public void onAddPlayerClicked(View view) {
		AlertDialog inputDialog = DialogFactory.getInputTextDialog(this,
				R.string.title_input_player, "", new OnClickListener() {
					@Override
					public void onClick(DialogInterface parent, int which) {
						if (_currentTeam != -1) {
							Dialog dialog = (Dialog) parent;
							PlayerContract player = new PlayerContract();
							EditText editText = (EditText) dialog.findViewById(R.id.edit_name);
							player.name = editText.getText().toString();
							player.role = 0;
							player.teamId = _currentTeam;
							player.order = _teamAdapter.getCount();
							_dbHelper.insertPlayer(player);
							_playerAdapter.changeCursor(_dbHelper
									.getAllPlayersForTeamCursor(_currentTeam));
							_playerAdapter.notifyDataSetChanged();
						}
					}
				}, null);
		inputDialog.show();
	}

	private void askForDeletingTeam(final long id) {
		TeamContract team = _dbHelper.getTeamById(id);

		AlertDialog alert = DialogFactory.getQuestionDialog(this,
				getString(R.string.title_delete_team),
				getString(R.string.message_delete_team, team.name), new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (id == _currentTeam) {
							viewTeam(-1);
						}
						_dbHelper.deleteTeam(id);
						_teamAdapter.changeCursor(_dbHelper.getAllTeamsCursor());
						_teamAdapter.notifyDataSetChanged();
					}
				}, null);
		alert.show();
	}

	private void renameTeam(final long id) {
		final TeamContract team = _dbHelper.getTeamById(id);

		AlertDialog alert = DialogFactory.getInputTextDialog(this, R.string.title_rename_team,
				team.name, new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Dialog d = (Dialog) dialog;
						EditText editText = (EditText) d.findViewById(R.id.edit_name);

						String newName = editText.getText().toString();
						if (newName.trim().length() != 0) {
							team.name = newName;
							_dbHelper.updateTeam(team);
							_teamAdapter.changeCursor(_dbHelper.getAllTeamsCursor());
							_teamAdapter.notifyDataSetChanged();
						}
					}
				}, null);
		alert.show();
	}

	private void renamePlayer(final long id) {
		final PlayerContract player = _dbHelper.getPlayerById(id);

		AlertDialog alert = DialogFactory.getInputTextDialog(this, R.string.title_rename_player,
				player.name, new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Dialog d = (Dialog) dialog;
						EditText editText = (EditText) d.findViewById(R.id.edit_name);

						String newName = editText.getText().toString();
						if (newName.trim().length() != 0) {
							player.name = newName;
							_dbHelper.updatePlayer(player);
							_playerAdapter.changeCursor(_dbHelper
									.getAllPlayersForTeamCursor(_currentTeam));
							_playerAdapter.notifyDataSetChanged();
						}
					}
				}, null);
		alert.show();
	}

	private void askForDeletingPLayer(final long id) {
		PlayerContract player = _dbHelper.getPlayerById(id);

		AlertDialog alert = DialogFactory.getQuestionDialog(this,
				getString(R.string.title_delete_player),
				getString(R.string.message_delete_player, player.name), new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						_dbHelper.deletePlayer(id);
						_playerAdapter.changeCursor(_dbHelper
								.getAllPlayersForTeamCursor(_currentTeam));
						_playerAdapter.notifyDataSetChanged();
					}
				}, null);
		alert.show();
	}

	private void setCaptain(long captainId) {
		for (int i = 0; i < _playerAdapter.getCount(); i++) {
			PlayerContract player = _dbHelper.getPlayerById(_playerAdapter.getItemId(i));
			player.role = player.id == captainId ? 1 : 0;
			_dbHelper.updatePlayer(player);
		}
		_playerAdapter.changeCursor(_dbHelper.getAllPlayersForTeamCursor(_currentTeam));
		_playerAdapter.notifyDataSetChanged();
	}
}
