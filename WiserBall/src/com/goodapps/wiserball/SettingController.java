package com.goodapps.wiserball;

import android.content.Context;
import android.content.SharedPreferences;

public class SettingController {
	private SharedPreferences _preferences;
	
	private static final String KEY_BALL_COUNT = "BallCount";
	private static final String KEY_WHITE_FIRST = "WhiteFirst";
	private static final String KEY_COUNT_DOWN = "CountDown";
	private static final String KEY_LANGUAGE = "Language";
	
	public static final int LANGUAGE_ENGLISH = 1;
	public static final int LANGUAGE_VIETNAMESE = 2;
	public static final int LANGUAGE_CHINESE = 3;
	
	private Context _context;
	
	public SettingController(Context context) {
		_context = context;
		_preferences = context.getSharedPreferences("WiserBall", Context.MODE_PRIVATE);
	}
	
	public void setBallCount(int ballCount) {
		_preferences.edit().putInt(KEY_BALL_COUNT, ballCount).commit();
	}
	
	public int getBallCount() {
		return _preferences.getInt(KEY_BALL_COUNT, 5);
	}
	
	public void setWhiteFirst(boolean isWhiteFirst) {
		_preferences.edit().putBoolean(KEY_WHITE_FIRST, isWhiteFirst).commit();
	}
	
	public boolean isWhiteFirst() {
		return _preferences.getBoolean(KEY_WHITE_FIRST, false);
	}
	
	public void setCountDown(int countDown) {
		_preferences.edit().putInt(KEY_COUNT_DOWN, countDown).commit();
	}
	
	public int getCountDown() {
		return _preferences.getInt(KEY_COUNT_DOWN, Integer.valueOf(_context.getString(R.string.default_count)));
	}
	
	public void setLanguage(int languageCode) {
		_preferences.edit().putInt(KEY_LANGUAGE, languageCode).commit();
	}
	
	public int getLanguage() {
		return _preferences.getInt(KEY_LANGUAGE, LANGUAGE_ENGLISH);
	}
}
