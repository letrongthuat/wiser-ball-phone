package com.goodapps.wiserball;

import java.io.File;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;

public class FileFragment extends Fragment {

	private ListAdapter _fileAdapter;

	private ListView _fileList;
	private ActionMode _actionMode;

	private OnItemLongClickListener _onFileItemLongClickListener = new OnItemLongClickListener() {
		@Override
		public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
			if (_actionMode != null) {
				return false;
			}
			_actionMode = getActivity().startActionMode(new MyCallback(R.menu.menu_file, position));
			view.setSelected(true);

			return true;
		}
	};

	private class MyCallback implements ActionMode.Callback {
		// private ActionMode.Callback _teamActionModeCallback = new
		// ActionMode.Callback() {
		private int _position;
		private int _menuResourceId;

		public MyCallback(int menuResourceId, int position) {
			_position = position;
			_menuResourceId = menuResourceId;
		}

		// Called when the action mode is created; startActionMode() was called
		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			// Inflate a menu resource providing context menu items
			MenuInflater inflater = mode.getMenuInflater();
			inflater.inflate(_menuResourceId, menu);
			return true;
		}

		// Called each time the action mode is shown. Always called after
		// onCreateActionMode, but
		// may be called multiple times if the mode is invalidated.
		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false; // Return false if nothing is done
		}

		// Called when the user selects a contextual menu item
		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			if (_menuResourceId == R.menu.menu_file) {
				switch (item.getItemId()) {
				case R.id.item_rename:
					renameFile(_position);
					mode.finish(); // Action picked, so close the CAB
					return true;
				case R.id.item_delete:
					askForDeletingFile(_position);
					mode.finish();
					return true;
				default:
					return false;
				}
			}
			return false;
		}

		// Called when the user exits the action mode
		@Override
		public void onDestroyActionMode(ActionMode mode) {
			_actionMode = null;
		}
	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_files, container, false);

		makeDir();

		String[] files = getFileList();
		_fileAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1,
				android.R.id.text1, files);

		_fileList = (ListView) v.findViewById(R.id.list_file);
		_fileList.setAdapter(_fileAdapter);
		_fileList.setOnItemClickListener((OnItemClickListener) getActivity());
		_fileList.setOnItemLongClickListener(_onFileItemLongClickListener);

		return v;
	}

	private String[] getFileList() {
		File folder = new File(Utils.getDataFolder());
		return folder.list();
	}

	private void makeDir() {
		File dir = new File(Utils.getDataFolder());
		if (!dir.exists())
			dir.mkdirs();
	}

	private void renameFile(final int position) {
		File folder = new File(Utils.getDataFolder());
		final String fileName = folder.list()[position];

		AlertDialog alert = DialogFactory.getInputTextDialog(getActivity(),
				R.string.title_rename_file, fileName, new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Dialog d = (Dialog) dialog;
						EditText editText = (EditText) d.findViewById(R.id.edit_name);

						String newName = editText.getText().toString();
						if (newName.trim().length() != 0) {
							File from = new File(new File(Utils.getDataFolder()), fileName);
							File to = new File(new File(Utils.getDataFolder()), newName.trim());

							if (!from.renameTo(to)) {
								DialogFactory.showMessageDialog(getActivity(),
										"Can not change the file name.");
								return;
							} else {

								_fileAdapter = new ArrayAdapter<String>(getActivity(),
										android.R.layout.simple_list_item_1, android.R.id.text1,
										getFileList());
								_fileList.setAdapter(_fileAdapter);
							}
						}
					}
				}, null);
		alert.show();
	}

	private void askForDeletingFile(final int position) {
		File folder = new File(Utils.getDataFolder());
		final String fileName = folder.list()[position];

		AlertDialog alert = DialogFactory.getQuestionDialog(getActivity(),
				getString(R.string.title_delete_file),
				getString(R.string.message_delete_file, fileName), new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						File file = new File(new File(Utils.getDataFolder()), fileName);

						if (!file.delete()) {
							DialogFactory.showMessageDialog(getActivity(),
									"Can not delete this file.");
							return;
						} else {

							_fileAdapter = new ArrayAdapter<String>(getActivity(),
									android.R.layout.simple_list_item_1, android.R.id.text1,
									getFileList());
							_fileList.setAdapter(_fileAdapter);
						}
					}
				}, null);
		alert.show();
	}
}
